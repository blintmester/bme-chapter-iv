
# Flutter-based Software Development

## Chapters


### Not yet updated for 2022/22/1

- [Chapter 1: Introduction](./material/01.md)
- [Chapter 2: Flutter basics](./material/02.md)
- [Chapter 3: Essential Flutter Widgets](./material/03.md)
- [Chapter 4: Asynchronous operations, State handling](./material/04.md)
- [Chapter 5: Advanced widgets, navigation, animation](./material/05.md)
- [Chapter 6: HTTP communication](./material/06.md)
- [Chapter 7: Persistent data storage](./material/07.md)

- [Chapter 9: Accessibility and responsive UI support](./material/09.md)
- [Chapter 11: Platform Channels and testing](./material/11.md)

## Requirements

 - Test, 40 points, minimum 40%
    - Last week of the term, at the time of the lecture, on Moodle (TBA - To Be Announced).
    - Sample test will be provided beforehand on Moodle.
    - There will be one retake available after the regular test.
    - The test will consist of the following:
      - 10 multiple choice questions (1 point each)
        - Wrong choices have negative points assigned to them
        - The result of a question cannot be negative
      - 5 short essay questions (2 points each)
        - Short answers, 4-5 sentences top will typically be enough.
      - 1 error search task (10 points)
        - The code with errors will be provided as an image
        - 1 error found counts as 1 point
        - Not every error has to be found for the maximum points (there will be more than 10 errors in the code)
      - 1 programming task (10 points)
        - A simple layout must be re-created with Flutter
        - Does not have to be syntactically correct, pseudo-code is fine
    - No tools are allowed during the test
  - Homework, 60 points, minimum 40%
    - Detailed requirements, deadlines, and suggested topics can be found [here](./material/homework.md)

## Resources
[Teams link](https://teams.microsoft.com/l/team/19%3a7d35dc7404c84ca1a1e022b39feac8ac%40thread.tacv2/conversations?groupId=102fff3a-4812-4499-92cb-dc5c3b847c1f&tenantId=6a3548ab-7570-4271-91a8-58da00697029)

## Extra materials recommended for reading and research

- [Flutter Complete Reference](https://fluttercompletereference.com/) - Alberto Miola

## The Flutter community

Most software platforms have an online community where people can share and discuss everything related to the topic, and Flutter is no exception.

Here are some places where you can meet fellow developers, or get the latest news on Flutter.

### Twitter

Twitter has become one of the most prominent platforms for online developer communities as you have great control over who you want to see content from.

Here are some accounts to follow (but there are many more!):

- [FlutterDev](https://twitter.com/FlutterDev)
- [Flutter Daily](https://twitter.com/flutteriodaily)
- [Felix Angelov](https://twitter.com/felangelov) - the author of the bloc libraries!
- [Matej Rešetár](https://twitter.com/resocoder) - AKA Reso Coder

### Reddit

// TODO
