class Santa {
  final String avatar;
  final String name;
  final List<String> outfit;
  final String description;

  Santa({
    this.avatar,
    this.name,
    this.outfit,
    this.description,
  });
}
