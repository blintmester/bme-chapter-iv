
# Homework

The homework is a mandatory requirement to pass the course. 60% of the course grade comes from the result of the homework.

## Application requirements

- The application must be written in Dart, using Flutter, and must support at least two different platforms with the same codebase:
  - Android
  - iOS
  - Web (responsive UI is a plus, but not mandatory)
  - Windows/macOS/Linux (only in alpha stage, choose at your own risk)
- The application must use a state management library (such as `provider` or `flutter_bloc`)
- Here are some strongly advised examples for app complexity:
    - Communicate with a public API
       - [A list of public APIs]([https://github.com/public-apis/public-apis](https://github.com/public-apis/public-apis))
       - One list page and one detailed page
       - Sorting based on some property OR searching based on some property
     - Simple game
       - Based on widgets or the [`flame`](https://pub.dev/packages/flame) library
       - Examples: 
           - TBA
     - Your own idea
       - Must be approved by one of the lecturers beforehand.

## Deadlines

 - A specification of the application must be submitted in .pdf format by the 8th week (TBA). The specification must be no longer than 1 page.
 - The completed project must be submitted by the end of the term (TBA).
 - The submitted file must contain the project (run `flutter clean` beforehand), and a short video demonstrating the completed application.
   - The video can also be provided as a link in a text file or in a comment on Moodle.
 - The homework can be submitted up to one week later for a compulsory fee.
