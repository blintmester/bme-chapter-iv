using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment
{
    public class LedDisplay : DisplayBase, IEquipment
    {
        public LedDisplay(int _creationYear, int _originalPrice, int _size, int _responseTime)
        {
            this.manufacturingYear = _creationYear;
            this.price = _originalPrice;
            this.size = _size;
            this.responseTime = _responseTime;
        }

        public double GetPrice()
        {
            return this.price;
        }
        public int GetAge()
        {
            return DateTime.Today.Year - this.manufacturingYear;
        }
        
        public string GetDescription()
        {
            return "LedDisplay";
        }


        int responseTime;
    }
}
