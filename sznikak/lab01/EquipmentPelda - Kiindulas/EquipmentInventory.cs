using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment
{
    class EquipmentInventory
    {
        private List<IEquipment> equipment;

        public EquipmentInventory()
        {
            equipment = new List<IEquipment>();
        }

        public void ListAll()
        {
            foreach (IEquipment eq in equipment)
            {
                Console.WriteLine($"Leírás: {eq.GetDescription()}\t" +
                                  $"Életkor: {eq.GetAge()}\tÉrtéke: {eq.GetPrice()}");
            }
        }

        public void AddEquipment(IEquipment eq)
        {
            equipment.Add(eq);
        }


    }


}
