using System;
using System.Xml.Serialization;

namespace lab02
{
    [XmlRoot("Személy")]
    public class Person
    {
        public event AgeChangingDelegate AgeChanging;
        
        private int age;

        [XmlAttribute("Kor")]
        public int Age
        {
            get
            {
                return age;
            }

            set
            {
                if (value < 0)
                {
                    throw new Exception("Érvénytelen életkor!");
                }

                if (AgeChanging != null)
                {
                    AgeChanging(age, value); // helyette: AgeChanging?.Invoke(age, value);
                }
                age = value;
            }
        }
        
        [XmlIgnore]
        public string Name { get; set; }
    }
}