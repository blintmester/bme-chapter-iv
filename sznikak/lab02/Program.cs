﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace lab02
{
    public delegate void AgeChangingDelegate(int oldAge, int newAge);
    
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            p.AgeChanging += PersonAgeChanging; // többszörös feliratkozás
            p.AgeChanging += PersonAgeChanging;
            p.Age = 2;
            p.AgeChanging -= PersonAgeChanging; // leiratkozás
            p.Age++;
            p.Name = "Luke";
            Console.WriteLine(p.Age);
            Console.WriteLine(p.Name);
            
            XmlSerializer serializer = new XmlSerializer(typeof(Person));
            FileStream stream = new FileStream("person.txt", FileMode.Create);
            serializer.Serialize(stream, p);
            stream.Close();
            
            // 5. feladat vagy mi
            
            List<int>  list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list = list.FindAll(MyFilter);
            
            for (int i = 0; i < list.Count; i++)
            {
                int n = list[i];
                Console.WriteLine($"Value: {n}");
            }
        }

        static bool MyFilter(int i)
        {
            return i % 2 == 1;
        }

        static void PersonAgeChanging(int oldAge, int newAge)
        {
            Console.WriteLine(oldAge + " => " + newAge);
        }
    }
}
