# Labor: Linux 2 elektronikus jegyzőkönyv

## Bevezetés

Ehhez a laborhoz használja az 1. Linux laboron létrehozott virtuális gépet!

## 1. feladat: A szerver teljesítményének mérése

A végleges weboldal feltöltése és publikálása előtt fontos kérdésként merül fel, hogy a létrehozott szerver hány kliens kiszolgálására képes? Fontos lenne megmérni, hogy a feltöltött egyszerű statikus weboldal másodpercenként hány felhasználó számára szolgálható ki, ugyanis ennek függvényében kell majd dönteni a frekventált időszakban az esetlegesen nagyobb teljesítményű VPS csomagra váltásról.

A teljesítmény mérésére az ab (Apache Benchmark) eszközt fogjuk használni. Az ab parancs alapvetően négy fontos paramétert vár, melyeket az alábbi kicsit pongyola jelentéssel lehet a legkönnyebben leírni:


 * az -n (number) kapcsolóval adhatjuk meg, hogy összesen hány kérést szeretnénk a szervernek elküldeni?
 * a -c (concurrency) kapcsolóval adhatjuk meg az, hogy egy-egy szimulált felhasználóhoz hány kérdés tartozzon az előbbiekben megadott összes kérés közül?
 * az -r kapcsoló megadásával utasíthatjuk a programot, hogy akkor is folytassa tesztelést, ha egyes kérdésekre nem kap választ a szerver túlterhelődése miatt
 * az utolsó paraméter (kapcsoló nélkül) a lekért oldal URL-je

A négy beállításból három viszonylag könnyen adódik: az -n kapcsoló értékét 50 000-re állítjuk, hogy a teszt néhány másodperc alatt lefusson, az -r kapcsolót bekapcsoljuk, hogy a teszt ne álljon le egy esetlegesen meg nem válaszolt kérés esetén, illetve az URL-nek a http://localhost/index.html URL-t használjuk majd. Azonban kérdés, hogy a felhasználónkénti kérésszámot mennyire állítsuk be?

Mivel egy oldalas statikus, egyetlen oldalas weblapról van szó, nem irreális feltételezés, hogy minden egyes kliens minden egyes a weboldalhoz tartozó fájlt le fog kérni a szerverről. Így tehát a -c paraméter meghatározásának kérdése visszavezethető arra, hogy hány fájl került feltöltésre a /var/www/html könyvtárba?

Első kísérletként kiadhatjuk az ls -l /var/www/html parancsot!

Ezzel azonban az a probléma, hogy így csak a legfelső könyvtárban található fájlokat és mappákat látjuk, a különböző .css, .js és kép fájlok rejtve maradnak.

Második kísérletnek használhatjuk az általánosan keresésre szolgáló find parancsot a find /var/www/html formában.

Ez már jobb megoldás, ugyanis kapunk egy listát az összes fájlról és könyvtárról, azonban számunkra a könyvtárak (pl. /var/www/html/js vagy /var/www/html) nem fontosak, hiszen azokat a böngésző nem tölti le a HTML oldal lekérésekor.

Hívja segítségül a man find parancsot annak érdekében, hogy megtalálja a megfelelő paraméterezést, amivel a find csak fájlokat listáz ki! A man program kimeneteként megjelenő használati utasításban a Page Up és Page Down gombokkal mozoghat, a / karakter leütése után kereshet, a kilépésre a q billentyű szolgál.

A fájlok összeszámolásához a find kimenetében szereplő sorokat kell megszámolni. Ez kézzel feleslegesen macerás lenne, szerencsére pont erre a célra szolgál a wc -l (word count) parancs úgy, hogy a find kimenetét a | (pipe) karakterrel a wc -l bemenetére vezetjük rá. A teljes parancsnak tehát find /var/www/html/ {az ön által meghatorozott további paraméter(ek)} | wc -l formájúnak kell lennie!

Készítsen képet a parancs kimenetéről, vagyis hogy hány fájl található a /var/www/html könyvtárban! Szövegesen indokolja, hogy az Ön által választott paraméter miért hagyja ki a könyvtárakat a find kimenetéből!

![1. kép](01find.png)

Válasz:
A `-type f` fájlokat ad vissza

Kiadott parancs:
`find /var/www/html/ -type f | wc -l`

Önellenőrzési pont: 

 * Ha 77-et vagy nagyobb számot kapott a fájlok számára az jó eséllyel hibás paraméterezeést jelent a find esetében! Ellenőrizze, hogy biztosan adott-e meg paramétert a find számára!
 * Ne felejtsen el indoklást is írni a parancshoz szöveges megjegyzésként, tehát magyarázza meg, hogy a megadott paraméter miért szűr csak a fájlokra!

Az előző parancs kimenetére (a fájlok számára) a továbbiakban {C} jelöléssel hivatkozunk a parancsokban!

A fentiek alapján adja ki az `ab -r -n 50000 -c {C} http://localhost/index.html` parancsot a {C} helyére az előbb leolvasott értéket helyettesítve!

Másodpercenként hány kérés kiszolgálására képes a szerverünk?

![2. kép](02ab.png)

Válasz:

2683.05 req/sec

## 2. feladat: Webáruház létrehozása

Az Önök által kifejlesztett termék jól debütált a nyilvánosság előtt és sikerült megfelelő befektetői hátteret találni, így eljutottak a sorozatgyártás fázisáig. Az értékesítés során fontos lenne egy jól működő webáruház, ahol közvetlenül a végfelhasználók is hozzáférnek a termékhez.

A különböző nyílt forráskódú webáruház rendszerek közül a választás végül az OpenCart rendszerre esik, melynek rendszerkövetelményei a következők:

    Web Szerver (Apache 2 ajánlott)
    PHP 5.4+
    Adatbázis (MySQLi ajánlott)

Az Ön feladata az, hogy biztosítsa a szükséges szoftver komponenseket, illetve ezt követően telepítse az OpenCart rendszernek a tárgy weblapjáról letölthető verzióját!

### 2.1. Előkészületek

Ha nem tudjuk az IP címét, akkor tegyük a következőket:

 * Jelentkezzünk be a VMWare Player konzol ablakában a korábban megadott hallgato felhasználói névvel és az oprepass jelszóval!
 * Adjuk ki az ip a parancsot, hogy meghatározzuk a virtuális gép aktuális IP címét, melyet az ens33 interfész inet paramétereként olvashatunk le. Jegyezzük meg ezt az IP címet, ugyanis ezt fogjuk használni mind az SSH kapcsolat felépítéséhez, mind pedig a böngészőben a weboldal lekéréséhez!

Az IP cím ismeretében:

 * Indítsuk el az SSH kliens programot a host gépen (pl. PuTTY), és jelentkezzük be a rendszerbe SSH kapcsolaton keresztül az előbb meghatározott IP címet, illetve az ismert felhasználói nevet és jelszót használva!
 * Írjuk be a host gépen egy webböngésző címsorába a virtuális gép IP címét, és győződjünk meg róla, hogy a virtuális gépben futó webszerver működik, vagyis megjelenik egy weboldal!

### 2.2. A PHP telepítése



A korábban telepített Apache2 webszerver egyelőre csak statikus fájlok kiszolgálására képes. Annak érdekében, hogy dinamikusan generált PHP nyelven íródott tartalmat is képes legyen értelmezni, szükség van a PHP modul telepítésére és engedélyezésére.

Az Apache PHP értelmező moduljának telepítéséhez használjuk a sudo apt install php libapache2-mod-php parancsot! A telepítés jóváhagyásának megerősítése előtt vessünk egy pillantást a csomagkezelő által megjelenített The following extra packages will be installed a sorra: jól látható itt, hogy az általunk megadott csomag mellett számos más függőséget felderített a csomagkezelő (a php-common a php parancsértelmező fő csomagja, a php7.4-cli egy parancssori frontend, stb.). Hagyjuk jóvá a telepítést az Enter lenyomásával!

A csomag telepítése automatikusan engedélyezte is a modult a webszerveren. A biztonság kedvéért szeretnénk ellenőrizni a PHP működését a működését – ehhez adjuk ki az echo ' <?php phpinfo(); ' > /var/www/html/test.php parancsot:

 * az echo parancs egyszerűen megjeleníti a kimenetén az argumentumként kapott szöveget
 * a > karakter az előtte található parancs kimenetét fájlba irányítja
 * így tehát az idézőjelek között szereplő rövid php kód bekerül a /var/www/html/test.php fájlba.

Ellenőrizzük, hogy a test.php fájl valóban létrejött-e a megfelelő tartalommal. Ehhez használjuk a less /var/www/html/test.php parancsot. Ha a paraméterként átadott fájl létezik, akkor az elinduló less megjelenítőből a q billentyű leütésével léphetünk ki.

Végezetül ellenőrizzük a webszerver által a frissen létrehozott fájlt lekérve megjelenített tartalmat! Ehhez a böngésző címsorába a virtuális gép IP címét követően írjuk be a /test.php végződést! Ha minden korábbi lépés megfelelően hajtottunk létre, akkor a phpinfo() függvény által visszaadott állapotjelentés látható.

Parancsok:
`sudo apt install php libapache2-mod-php`

`echo ' <?php phpinfo(); ' > /var/www/html/test.php`

`less /var/www/html/test.php`

![3. kép](03php.png)

Ellenőrizzük a telepített PHP verziószámát a megjelenő oldal fejlécében, illetve az Apache webszerver verziószámát az Apache Version kulcsszóra rákeresve, hogy mindkét paraméter megfelel-e az OpenCart követelményeinek!

### 2.3. A MySQL adatbázis szerver telepítése

Az OpenCart utolsó hiányzó előkövetelménye egy adatbázis szerver, melyet a javaslatnak megfelelően MySQL szervernek választunk.

A MySQL telepítéséhez adjuk ki a sudo apt install mysql-server php-mysql parancsot, mely egyben a PHP illesztést is telepíteni fogja!

Ellenőrizzük a telepítés sikerét és a szerver működőképességét a sudo mysql -u root parancs kiadásával! A sikeres belépés után megjelenik a mysql kliens konzolja, ahol közvetlenül SQL utasításokkal vezérelhetjük a szervert.

Az OpenCart telepítéséhez szükségünk lesz egy üres adatbázisra illetve egy ahhoz hozzáférő felhasználóra. Ezek előkészítéséhez adjuk ki a MySQL konzoljában a következő SQL parancsokat egymás után:

CREATE DATABASE opencart;
CREATE USER 'opencartuser'@'localhost' IDENTIFIED BY 'openpass';
GRANT ALL PRIVILEGES ON opencart.* TO 'opencartuser'@'localhost';

Ellenőrizzük, hogy az előző parancsok mindent megfelelően beállítottak:

 * Az adatbázis létrejött-e: show databases;
 * A felhasználó létrejött-e: select Host, User from mysql.user; 
 * A felhasználó megkapta-e a szükséges jogokat: show grants for opencartuser@localhost;

Parancsok:
`sudo apt install mysql-server php-mysql`

`sudo mysql -u root`

A mysql-ben:

```
CREATE DATABASE opencart;
CREATE USER 'opencartuser'@'localhost' IDENTIFIED BY 'openpass';
GRANT ALL PRIVILEGES ON opencart.* TO 'opencartuser'@'localhost';
```

és ellenőrzés:
```
show databases;
select Host, User from mysql.user; 
show grants for opencartuser@localhost;
```

Készítsen egy képet a MySQL konzoljáról a három ellenőrző parancs kiadása után!

![4. kép](04mysql.png)

Önellenőrzési pont: Értelmezze az ellenőrző lekérdezések kimenetét! Ha bármi hiányzik (az adatbázis, a felhasználó vagy a felhasználó jogai), lépjen vissza a létrehozó parancsokhoz!

A MySQL konzoljából az exit paranccsal léphet ki!

### 2.4. Az OpenCart telepítése

A telepítéshez először az OpenCart forráskódját kell a szerverre feltöltenünk. Ehhez:

 1. A terminálban navigáljunk ugyanebbe a könyvtárba a cd /var/www/html parancs kiadásával!
 2. Töröljük a korábbi egy lapos weboldalt a rm index.html paranccsal!
 3. Az OpenCart forráskódját tartalmazó ZIP fájl letöltéséhez adjuk ki a wget -O opencart.zip http://home.mit.bme.hu/~eredics/opre/opencart-2021.zip parancsot!
 4. Tömörítsük ki a fájlt az unzip -o opencart.zip parancs kiadásával!
 5. Távolítsuk el a ZIP fájlt az rm opencart.zip parancs segítségével.
 6. Adjunk át a fájlokat a webszerver www-data nevű felhasználójának a sudo chown -R www-data /var/www/html paranccsal!

Amennyiben a feltöltés sikeres, akkor írjuk be a böngészőnkbe a virtuális gép IP címét, aminek hatására megjelenik a rendszer grafikus telepítőjének kezdő oldala. (Ha a korábbi weboldal jelenik meg, akkor ellenőrizzük, hogy a fenti 2-es lépésben valóban töröltük-e az index.html fájlt, illetve a böngésző Ctrl+F5 billentyűkombinációjával kérjük az oldal gyorsítótár nélküli újratöltését!)

Fogadjuk le a licenc feltételeket, vagyis kattintsunk a Continue gombra!

A második lépésben a telepítő ellenőriz a szükséges rendszer követelmények teljesülését, azonban néhány PHP könyvtár esetén hibát jelez. A hiányzó könyvtárak telepítéséhez használjuk a következő parancsokat:

 * `sudo apt install php-curl php-gd php-zip` a csomagok telepítéséhez.
 * `sudo service apache2 restart` a webszerver újraindításához.

Végül töltsük újra a böngészőben az oldalt! Ha minden követelmény teljesül, akkor kattintsunk a Continue gombra!

A telepítés 3. lépésében adjuk meg az adatbázis hozzáféréshez szükséges adatokat:

 * Username: opencartuser
 * Password: openpass
 * Database: opencart

Az OpenCart admin felhasználójának jelszava legyen admin, e-mail címnek tetszőleges e-mail cím megadható. Végül kattintsunk a Continue gombra!

Az utolsó lépés a sikeres telepítésről tájékoztat, illetve figyelmeztet, hogy ne felejtsük el a telepítő install könyvtárát törölni a szerverről. Ez utóbbit a `sudo rm -r /var/www/html/install` parancs kiadásával végezhetjük el, majd a böngészőben lépjünk tovább a frissen telepített webshopunk főoldalára!

Parancsok:
`cd /var/www/html`

`rm index.html`

`wget -O opencart.zip http://home.mit.bme.hu/~eredics/opre/opencart-2021.zip`

`unzip -o opencart.zip`

`rm opencart.zip`

`sudo chown -R www-data /var/www/html`

Fogadjuk le a licenc feltételeket, vagyis kattintsunk a **Continue** gombra!

`sudo apt install php-curl php-gd php-zip `

`sudo service apache2 restart`

`sudo rm -r /var/www/html/install`

Készítsen egy képet a frissen létrehozott webáruházról a böngészőjéből!

![5. kép](05store.png)

## 3. feladat: Teljesítménymérés újra

A szerverünkre most telepített OpenCart webshop jóval komplexebb rendszer, mint a korábban üzemeltetett egyetlen oldalból álló weblapunk, így felmerül kérdésnek, hogy hogyan változott a szerverünk teljesítménye ennek hatására?

Újabb mérés futtatásához adjuk ki a virtuális gép konzolján az ab -r -n 5000 -c 33 http://localhost/index.php parancsot! Most már jóval lassabban érkeznek a válaszok.

A szűk keresztmetszet meghatározásához használjuk az SSH kapcsolaton keresztül éppen nem foglalt konzolunkat és először telepítsük az atop nevű parancsot a sudo apt install atop parancs segítségével! Ezt követően adjuk ki az atop parancsot!

Az atop az első 10 másodpercben a rendszerindítás óta összesített statisztikát mutat, majd 10 másodpercenként frissítve az aktuális terhelési adatokat jeleníti meg. A fejlécben a processzor (CPU, CPL), memória (MEM, SWP), a lemez (DSK) és a hálózat (NET) összesített adatait látjuk színesen kiemelve a potenciális problémákat. A konzol alsó részén az időszeletben aktív folyamatok láthatók CPU használat szerint csökkenő sorrendben. (További információkat az atop manual oldalán találhat.)

Parancsok:

`ab -r -n 5000 -c 33 http://localhost/index.php`

`sudo apt install atop`

`atop`

Készítsen egy képet az atop futásáról miközben az ab terhelést generál a szervernek!

![6. kép](06atop.png)

Az atop kimenete alapján a rendszer mely komponense a szűk keresztmetszet?

CPU

Önellenőrzési pont: Ha az atop kimenetében a CPU soron az idle felirat mellett 90+%-os értéket lát, akkor a kép készítésének pillanatában a processzor az idő nagyrészében tétlen volt. Ez azt jelenti, hogy nem volt terhelés alatt a rendszer amikor a képet készítette! Ellenőrizze, hogy az ab fut-e a háttérben, és készítsen új képet!

## 4. feladat: Tűzfal beállítása

A szerver az eddigi beállítások mellett az intereneten tetszőleges helyről fogad minden nyitott portjára kapcsolódó klienseket. Nem praktikus ezt a lehetőséget a teljes internet számára nyitva hagyni, különösen, hogy a szabványos portokat nagyon sűrűn érik automatizált támadási kísérletek. Ezek kivédésére fontos a frissítések megfelelően gyakori telepítése, azonban ezen túl is praktikus a támadási felületet minimalizálni egy megfelelően beállított tűzfal használatával - így ha később bármilyen alkalmazás portot nyit a szerveren, akkor a tűzfal még mindig egy közbenső védelmi réteget jelent a támadók felé.

A Linux rendszereken legelterjedtebb tűzfal megoldás **Iptables** névre hallgat, ez megtalálható a telepített Ubuntu operációs rendszerben is. Az aktuális tűzfal szabályokat a sudo iptables -L paranccsal jelenítheti meg. Ahogy a parancs kimenetén látszik, jelenleg egyetlen tűzfal szabály sem aktív. A pillanatnyilag nyitott portokat a netstat parancs listázza. Első lépésben telepítse a szükséges csomagot (sudo apt install net-tools), majd a nyitott portokat a sudo netstat -lpn paranccsal listázhatja ki. (A kapcsoló elején kis L betű van.)

Parancsok:

`sudo iptables -L`

`sudo apt install net-tools`

`sudo netstat -lpn`

Készítsen képet az iptables és a netstat kimenetéről! 

![7. kép](07iptables.png)

A parancs kimenetén a Local Address oszlopban háromféle IP cím szerepel:

 * 0.0.0.0 vagy ":::": az itt nyitott portok minden hálózati interfészen elérhetőek
 * 127.0.0.1 (localhost): az itt nyitott portok csak a helyi gépről hozzáférhetőek
 * a rendszer külső IP címe: az itt nyitott portok kívülről illetve belülről is az adott IP címen nyitottak

Ahogy látszik a külső interfészen a 22-es és 80-as portok nyitottak. A netstat kimenetből az is kiderül, hogy a 22-es az SSH szerver, ami a távoli menedzselhetőség érdekében indokoltan van nyitva, míg a 80-es portok a webszerverhez tartozik.

Az iptables közvetlen konfigurálása komplex feladat, azonban szerencsére a rendszer egy ufw (Uncomplicated Firewall) frontend komponenst tartalmaz, ami jelentősen megkönnyíti a konfigurálást. Először adjuk meg az ufw-nek a szabályokat az alábbi formában:

`sudo ufw allow ssh`

`sudo ufw allow http`

`sudo ufw enable`

Az első két parancs feltétel nélkül engedélyezte az SSH és HTTP kapcsolatokat, míg az utolsó bekapcsolja a tűzfalat. Ezt követően a sudo iptables -L ismételt kiadása megjeleníti a céljainknak megfelelő szabályrendszert az iptables által elvárt formában. Az ufw jóval átláthatóbb megfogalmazását ami alapján a komplex iptables szabályok generálódtak a sudo ufw status parancs jeleníti meg.

Parancsok:

`sudo ufw allow ssh`

`sudo ufw allow http`

`sudo ufw enable`

Készítsen képet az ufw status parancs kimenetéről!

![8. kép](08ufw.png)

Ezen a ponton a megfelelt teljesítéshez szükséges feladatok végére ért.
Amennyiben a laboron nem szeretne kiválóan megfelelt értékelést szerezni, akkor a további válasz mezőkbe egy-egy "-" karakter bejegyzésével jelezze, hogy a jegyzőkönyv értékelésével ne várjunk az extra feladatok megoldására!

## 5. feladat: Saját felhő tárhely létrehozása* (csak a kiválóan megfelelt értékeléshez)

A cégnél egyre több alkalmazott dolgozik több különböző eszközön is (például PC-n és az irodán kívül laptopon), és egyre nagyobb problémát jelent az egyes gépek között az adatok és dokumentumok szinkronizálása. Számos piaci megoldás létezik e probléma megoldására (pl. Google Drive, Dropbox, stb.), azonban a céges belső szabályzat tiltja, hogy érzékeny anyagok külső szolgáltató szerverére kerüljenek fel. Az Ön feladata, hogy létrehozzon egy hasonló funkcionalitású rendszert helyben!

Az előírt feladatok ellátására alkalmas nyílt forráskódú megoldás nextCloud névre hallgat, és mindössze egy webszerver, PHP és egy MySQL adatbázis szükséges a működtetéséhez – amelyek mindegyikével már rendelkezik az Ön által menedzselt virtuális gép. A nextCloud telepítés nagyban hasonlít az 1. feladat végén végrehajtott OpenCart telepítésre, ugyanis ez esetben is a következő lépéseket kell végrehajtania:

 * Töltse le a nextCloud forráskódját innen a szerver /var/www/html könyvtárába!
   * Ehhez az 1.4. feladatban talál segítséget.
 * Tömörítse ki a felmásolt ZIP fájlt, majd ezt követően törölje is a fájlt!
   * Ehhez szintén 1.4. feladat szolgálhat mintával.
 * Állítsuk be a könyvtárra a webszervert futtató www-data felhasználót tulajdonosnak:

`sudo chown -R www-data /var/www/html/nextcloud`

 * Hozzon létre egy MySQL adatbázist nextcloud néven, és egy felhasználót nextclouduser néven nextcloudpass jelszóval!
   * Ehhez az 1.3. feladatban talál mintát.

 * Töltse be a host gépen a böngészőben a virtuális gép IP címét /nextcloud végződéssel!
 * A telepítő néhány modult hiányol! A Google segítségével határozza meg, hogy mely csomagokat kell az operációs rendszerre telepíteni a hiányzó modulok pótlására!
   * A csomagok telepítése után ne felejtse el az Apache webszervert újraindítani (sudo service apache2 restart)!
   * Ismételje ezt a lépést addig, amíg a nextCloud telepítője minden függőségét meg nem találja, és tovább nem lép a rendszergazdai fiók létrehozására!
 * A rendszergazdai fiók létrehozása felületen adja meg a szükséges adatokat:
   * A felhasználó neve legyen az Ön Neptun kódja, jelszava legyen admin!
   * Az adat könyvtárat ne módosítsa!
   * Az adatbázis eléréshez használja az előbbiekben létrehozott adatbázis és felhasználó nevet valamint jelszót!

A megfelelő beállítások után várjunk türelemmel a telepítés végéig, majd megjelenik a rendszer webes felülete.

Parancsok:

`wget http://home.mit.bme.hu/~eredics/opre/nextcloud-2021.zip`
`unzip -o nextcloud-2021.zip`
`rm nextcloud-2021.zip`
`sudo chown -R www-data /var/www/html/nextcloud`

MySQL:

`sudo mysql -u root`

```
CREATE DATABASE nextcloud;
CREATE USER 'nextclouduser'@'localhost' IDENTIFIED BY 'nextcloudpass';
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextclouduser'@'localhost';
exit;
```

Error kezelés:

`sudo apt install php-xml`
`sudo service apache2 restart`

Készítsen képet a saját felhasználójával bejelentkezés után nextCloud webes felületéről úgy, hogy a képen látható legyen Neptun kódja! (Látható Neptun kód nélkül a feladat nem elfogadható!)

![9. kép](09nc.png)

Önellenőrzési pont: Ellenőrizze, hogy a fenti képet valóban bejelentkezés után készítette-e, illetve, hogy azon látszik-e a felhasználói neve (Neptun kódja), ugyanis a feladat csak így fogadható el!