# Labor: Linux 1 elektronikus jegyzőkönyv

## Jelmagyarázat

A mérési utasításban az egyes lépésekben szükséges választásokat illetve beírandó parancsokat <span style="color?:blue">**kék félkövér**</span> szöveg jelzi. Ahol ezekre a választásokra vagy parancsokra csak hivatkozás történik, ott **fekete félkövér** betűtípust használunk. Ezen a laboron nem lehet kiválóan megfelelt minősítést szerezni.

## Bevezetés

Néhány fiatal mérnök társa kitalált egy terméket, melyet érdemesnek tartanak arra, hogy egy startup vállalkozást hozzanak létre rá. Ebben a csapatban mindenki egy-egy specifikus feladaton dolgozik: van, aki a termék fejlesztéséért felelős, más a befektetők felkutatását intézi, megint más a grafikai elemekért és a marketingért felelős, míg az Ön feladata az informatikai háttér biztosítása. A projekt kezdeti fázisában mindent a résztvevők finanszíroznak, ezért a költségkeret igen szűkös, ráadásul ennek a nagy részét is a konkrét termék fejlesztése emészti fel, így az Ön feladataira minimális erőforrások állnak rendelkezésre – azonban fontos az éppen induló vállalkozás hírneve szempontjából, hogy kizárólag jogtiszta forrásból származó eszközöket használjon fel.

A prototípus fejlesztése eljutott abba a fázisba, hogy szeretnék azt a befektetők meggyőzése érdekében és az érdeklődés felmérése céljából bemutatni a nagyközönségnek. Ehhez egy napjainkban divatos egy oldalas weboldalt fog majd létrehozni a csapat grafikus tagja, azonban az ezt kiszolgáló szerver létrehozása az Ön feladata!

A feladat megoldása során két fontos szempontot kell figyelembe vennie: egyrészt a költségkeret nagyon szűkös, így saját szerver fenntartása szóba se kerülhet, másrészt pedig az első sajtóközlemény közzététele után hirtelen nagy érdeklődés várható, tehát a rendszernek minimum időszakosan jól skálázhatónak kell lennie. E peremfeltételek mentén az a döntés születik, hogy a cég egy virtuális szervert (VPS) bérel, mely havi 5$ (~1500 Ft) körüli költségen tipikusan egy processzormagot, 0,5-1 GB memóriát és 10-20 GB háttértárat tartalmaz. A legtöbb szolgáltató ezeket a termékeket időalapon számlázza, ez a megoldás tehát lehetőséget biztosít az alatta működő hardver gyors és átmeneti jelentős bővítésére úgy, hogy a megnövekedett költségeket csak az érintett időszakban kell fizetni.


## 1. feladat: VPS szolgáltató keresése

Keressen az Interneten a közép-európai régióban a fent megadott paramétereknek megfelelő VPS szolgáltatót és szolgáltatási csomagot!

### Megjegyzés:

Amik kellenek:

 * 5$/hó
 * 1 processzor mag
 * 0.5-1 GB RAM
 * 10-20 GB tárhely
 * közép-európai szolgáltató

![1. kép](01vps.png)

**Válasz:**

A rackforest-et választottam, mert minden követelménynek megfelelt.

Önellenőrzési pont: Továbblépés előtt ellenőrizze, hogy... 

 * Az Ön által választott VPS szolgáltató valóban rendelkezik-e közép-európai adatközponttal!
 * A választás során nem lépte-e túl jelentősen a megengedett 5 $-os havi költségkeretet!
 * A beszúrt képen jól látszanak-e a választott VPS paraméterei (CPU szám, memória mérete, háttértár mérete), ugyanis a második feladatban szüksége lesz ezekre!

## 2. feladat: Virtuális gép létrehozása



Miután előfizettek a virtuális szerverre úgy döntenek, hogy először minden beállítást és módosítást egy a VPS-sel paramétereiben megegyező virtuális gépen fognak tesztelni annak érdekében, hogy az éles rendszer működése problémamentes legyen. Az Ön feladata tehát, hogy létrehozzon egy a fenti VPS-sel paramétereiben megegyező virtuális gépet a VMWare Player program segítségével!

Töltse le (letöltés link, oktatási célra ingyenes), telepítse majd nyissa meg a VMWare Player programot, és válassza a Create a New Virtual Machine lehetőséget!

A virtuális gép létrehozása során:

* Egyelőre ne adjon meg telepítési médiumot („I will install the operating system later”)!
 * Válasszon Linux operációs rendszert, és azon belül Ubuntu-t!
 * Laborgépen dolgozva ügyeljen, hogy a virtuális gép diszk fájlját a D:\ meghajtó alatt egy tetszőleges mappában hozza létre! Saját gépen dolgozva ennek nincs jelentősége.
 * A lemez méretét állítsa be a választott VPS csomagnak megfelelően!
 * A virtuális gép létrehozása után a beállítások között állítsa be a CPU magok számát és az elérhető memóriát a választott VPS csomagnak megfelelően!

![2. kép](02settings.png)

Önellenőrzési pont: Továbblépés előtt ellenőrizze, hogy...

 * A létrehozott virtuális gép paraméterei (HDD méret, RAM mért) biztosan megegyeznek-e az 1. feladatban kiválasztott csomaggal! A virtuális géppel történő tesztelés során fontos szempont, hogy ha a VPS-nek valamilyen erőforrás limitációja előjönne a valóságban, akkor az lehetőleg már a teszt során kiderüljön!
 *  A beszúrt képen látszanak-e a létrehozott virtuális gép releváns paraméterei (CPU magok száma, memória mérete, háttértár mérete)!

## 3. feladat: Operációs rendszer telepítése



A rendszer szűkös erőforrásai illetve az anyagi korlátok miatt szóba se jöhet Windows operációs rendszer használata, ellenben a különböző Unix alapú rendszerekből igen nagy a választék. A különböző variánsok és disztribúciók közül az Ön választása az egyik legelterjedtebb disztribúcióra az Ubuntu-ra, azon belül pedig a 2021. elején legfrissebb 20.04-es LTS (Long Term Support, vagyis 10 évig garantáltan támogatott) verzióra esik.

A telepítés elindításához a következőket kell tennie:

 * Töltse le a tárgy weblapjáról (innen) a telepítő lemez mini ISO fájlját (Ubuntu-20.04-LTS-mini.iso)!
 * Indítsa el a virtuális gépet!
 * A VMWare Player program Player menüjéből a Removable devices pont alatt válassza ki a CD/DVD pontot, azon belül pedig a Settings oldalt!
 * A megjelenő beállító oldalon válassza a „Use ISO image file” lehetőséget, és adja meg az imént letöltött ISO fájl elérhetőségét!
 * Pipálja be a Connected és a Connected on power on dobozokat!

A beállítások elvégzése után indítsa újra a virtuális gépet, mely ekkor a telepítő lemezről bootol be, és annak főmenüjét jeleníti meg.  

A lemez menüjéből válassza az Install pontot, majd telepítse az operációs rendszert!  

### Telepítés önállóan

Amennyiben Önnek már van tapasztalata Linux operációs rendszerek telepítésével kapcsolatban, akkor a telepítést önállóan is elvégezheti az alábbi részletes leírás tanulmányozása nélkül. A telepítés során a következő paraméterekben kell eltérnie az alapértelmezetten felajánlott értékektől (az alábbi beállításoktól eltérni tilos!):

 * A telepített rendszer nyelve legyen angol!
 * A gép hosztneve legyen a saját neptun kódja!
 * A felhasználó (megjelenítéshez használt) teljes neve legyen hallgato, a hozzáféréshez használt bejelentkezési név is legyen hallgato, a jelszó pedig legyen oprepass. A nevet és jelszót is csupa kisbetűvel adja meg!
 * Engedélyezze az automatikus frissítések telepítését, vagyis válasza az Install security updates automatically pontot!
 * Az előtelepített csomagok közül egyet se válasszon ki (még a Basic Ubuntu Server lehetőséget se), vagyis a Choose software to install kérdésre nyomjon TAB után Continue-t!

### Telepítés után

A telepítő futtatása után a virtuális gép újraindul és a frissen telepített rendszer bootol be. A bejelentkező konzol megjelenése után a telepítés során megadott hallgato névvel és oprepass jelszóval léphetünk be. 

Készítsen egy képet az első belépés után a konzolról!

![3. kép](03login.png)

Önellenőrzési pont: Továbblépés előtt ellenőrizze, hogy...

 * A létrehozott gép hostneve az Ön Neptun kódja, vagyis ez látszik a belépéskor a terminálon! A további feladatokat csak akkor fogadjuk el, ha a képernyőképeken az Ön neptun kódja látszik a hostnév helyén! Más hostnév használata esetén ("ubuntu", "vm", stb.) a további feladatokat nem értékeljük!

Az első dolgunk az elérhető frissítések ellenőrzése. Ehhez adjuk ki a sudo apt update parancsot, mellyel a csomagkezelő adatbázisát aktualizálhatjuk. Ezt követően futtassuk a sudo apt upgrade parancsot, mely jelen esetben jó eséllyel egyetlen csomagot se frissít majd, ugyanis a telepítő mindenből a legfrissebb változatot töltötte le a telepítés során!

Készítsen egy képet az apt upgrade kimenetéről!

![4. kép](04upgrade.png)

## 4. feladat: SSH szerver telepítése

A rendszer konzoljának elérése általában nehézkes és rugalmatlan, fizikai szerverek esetében sokszor nem is csatlakozik monitor és billentyűzet az adott géphez. Telepítsünk tehát egy biztonságos távoli elérést lehetővé tevő SSH (Secure Shell) szervert!

Ehhez először adjuk ki az sudo apt install openssh-server parancsot! A telepítés után az SSH kapcsolat létrehozásához ismernünk kell a virtuális gép IP címét. Ennek meghatározásához adjuk ki az ip a parancsot a konzolon, majd keressük ki az ens33 interfész inet nevű paraméterénél az IP címet!

Jegyezze fel ide a virtuális gép IP címét! Figyeljen arra, hogy az alhálózati maszk nem része az IP címnek!

`192.168.92.128`

Önellenőrzési pont: Továbblépés előtt ellenőrizze, hogy...

 * Az IP címmel együtt nem másolta-e be a fenti mezőbe a netmaszkot, ugyanis a továbbiakban a virtuális gép eléréséhez csak az IP címre lesz szükségünk, a címet követő (például /24) netmaszkra nem!

A host gépen használjunk egy SSH kliens programot (pl. PuTTY - ha nem elérhető a host gépen, akkor hordozható változatban letölthető innen), és adjuk meg a célcímnek a virtuális gép előbb meghatározott IP címét! Az első kapcsolódás alkalmával a kliens program figyelmeztet, hogy korábban ehhez a géphez még nem kapcsolódtunk, így annak kulcs lenyomata alapján nem garantálható, hogy valóban azzal a szerverrel kommunikálunk, amivel szeretnénk. Esetünkben nem áll fenn közbeékelődős támadás veszélye, így nyugodtan továbbléphetünk a lenyomat elfogadásával. Ezt követően lépjünk be a hallgato névvel és oprepass jelszóval!

Készítsen egy képet az SSH belépés után a kliens ablakáról!

![6. kép](06ssh.png)

A továbbiakban javasoljuk, hogy a konzol helyett az SSH kliens programban dolgozzon, mert oda könnyen bemásolhat parancsokat (a PuTTY programba a vágólap tartalmát az egér jobb gombjával kattintva szúrhatja be), illetve szükség esetén tetszőlegesen sok nyitható belőle egymás mellett!

## 5. feladat: Webszerver telepítése

A rendszer telepítésének célja egy egyszerű statikus weboldal kiszolgálása volt, így feltétlenül szükség van egy webszerver telepítésére.

A leginkább széles körben használt Apache2 webszerver telepítéséhez adjuk ki a sudo apt install apache2 parancsot! A telepítés lefutása után nyissunk egy böngészőt, és a cím mezőbe írjuk be a virtuális gép IP címét! Az Apache2 alapértelmezett telepítés utáni weboldalának kell megjelennie a böngészőben!

![7. kép](07apache.png)

Ahogy az alapértelmezett weboldal is feltünteti, a szerver által kiszolgált fájlok a /var/www/html könyvtárban találhatóak. Az ls -la /var/www/html parancs kiadásával tekintse meg ennek a könyvtárnak a tartalmát a terminálon!

A parancs kimenetéből jól látható, hogy milyen fájlt szolgál ki a webszerver, illetve az is látszik, hogy mind ennek a fájlnak, mind magának a teljes html könyvtárnak a root felhasználó illetve a root csoport a tulajdonosa. Mivel a későbbiekben nem a root felhasználó nevében szeretnénk írni ezt a könyvtárat az egyszerűség kedvéért vegyük saját tulajdonba a sudo chown -R hallgato:hallgato /var/www/html parancs kiadásával!

Mint számos más unix parancs, sikeres lefutáskor a chown se jelenít meg semmit a konzolon, csak hiba esetén kapunk üzenetet. Ellenőrzésképpen adjuk ki újra az ls -la /var/www/html parancsot, és győződjünk meg a jogosultságok sikeres módosításáról!

Készítsen egy képet a html könyvtár tartalmáról!

![8. kép](08ls.png)

## 6. feladat: A weboldal feltöltése



Mivel a grafikus még nem készült el a végleges weblappal, ezért első körben egy előzetes sablont küldött Önnek, hogy a szerveren ne az alapértelmezett weboldal jelenjen meg. Ezt a sablont innen töltheti le static-html-website.zip néven.

Az SSH szerver alapértelmezetten úgy van beállítva, hogy SFTP szerverként is funkcionál. Így mindössze egy SFTP kliens programra van szükséges (pl. WinSCP - ha nem elérhető a host gépen, akkor letölthető innen) a kapcsolódáshoz. A host name mezőbe a szerver IP címét írja, míg a felhasználói nevet és jelszót az SSH illetve a konzolos bejelentkezéshez használt értékekkel töltse ki!

Az SSH biztonsági figyelmeztetése itt is megjelenik az első csatlakozás alkalmával, erre nyugodtan nyomjon Yes választ! A sikeres kapcsolódás után keresse ki a /var/www/html könyvtárat, és másolja fel a tárgy weblapjáról letöltött static-html-website.zip fájlt!

Az utolsó lépés a felmásolt ZIP fájl kitömörítése. Ehhez lépjen be a megfelelő könyvtárba a cd /var/www/html parancs kiadásával, majd adja ki az unzip -o static-html-website.zip parancsot!

A parancs valamiért meghiúsul. Miért? Értelmezze a hibaüzenetet, és oldja meg a felmerült problémát önállóan!

![9. kép](09oxygen.png)