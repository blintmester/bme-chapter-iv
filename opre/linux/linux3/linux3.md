# Labor: Linux 3 elektronikus jegyzőkönyv

## Bevezetés

A Linux 3-as labor feladatai függetlenek a korábbi Linux 1-2 laboroktól. Ebben a laborban egy grafikus felülettel rendelkező asztali Linux operációs rendszert telepítünk majd és ezen vizsgáljuk a rendszer felépítését és komponenseit, illetve azok testreszabhatóságát.  

## 0. feladat: Nyilatkozat a használt rendszerről

Ha Ön otthon dolgozik a labor feladatain és a saját számítógépén már most is egy asztali Linux operációs rendszert használ, akkor az Ön számára az 1. fejezet telepítési feladatai nem kötelezőek, használhatja a saját gépén már telepített Linux rendszert. Ebben az esetben az egyes konkrét beállítások eltérhetnek az Ön által használt disztribúción, így bizonyos parancsok nem teljesen azonos formában vagy paraméterezéssel használandók - ilyen esetben a megfelelő megoldást Önnek kell megkeresnie! Ha élni szeretne ezzel a lehetőséggel, akkor jelezze ezt az alábbi kérdésre adott válaszában, továbbá a telepítési feladatok megoldás mezőjében egy-egy "Saját rendszeren dolgozok." megjegyzéssel jelezze, hogy ezeket a kérdéseket Önnek nem kell megválaszolnia!

Amennyiben már telepített, saját Linux operációs rendszerén dolgozik, írja le az Ön által használt konkrét disztribúciót és annak verzióját! Egyéb esetben írjon "-"-t a válasz mezőbe!

Saját rendszeren dolgozok.

## 1. Feladat: A Linux Mint 20.1 "Ulyssa" telepítése

Ezt skippeltem.

## 2. feladat: Ismerkedés a telepített szoftverekkel

A legtöbb asztali Linux disztribúció számos nyílt forráskódú programot előre telepítve tartalmaz.

### 2.1. A rendszerrel együtt telepített alkalmazások

Tekintse át a képernyő bal alsó sarkából elérhető Menüből elérhető programokat! A legtöbb hétköznapi feladatra (böngészés, levelezés, szövegszerkesztés, stb.) talál előre telepített alkalmazást a rendszerben.

### 2.2. A Szoftverkezelő használata

Ha további asztali alkalmazásokat szeretnénk telepíteni (pl. Skype kliens), akkor erre használhatjuk a Szoftverkezelőt, mely a rendszer menüben az Adminisztráció pont alatt érhető el.

 * Nyissa meg a Szoftverkezelőt!
 * Telepítse a Gparted nevű partíciókezelő programot a Szoftverkezelő használatával a Telepítés gombot használva!
 * Indítsa el az alkalmazást az Indítás gombbal! A program az elérhető egyetlen lemez (/dev/sda) partícióit mutatja.

Készítsen egy képet a Gparted felületéről!

![5. kérdés](05gparted.png)

### 2.3. A csomagkezelő használata

A Szoftverkezelő segítségével egyszerűen telepíthető a legtöbb gyakori alkalmazás az összes szükséges függőségével együtt. Ha nagyobb kontrollt szeretnénk a telepített csomagok felett, akkor erre a célra a Csomagkezelő szolgál, melyet a rendszer menü Adminisztráció pontja alól érhet el.

 * Nyissa meg a Synaptic csomagkezelőt!
 * A World Wide Web kategóriában válassza ki az apache2 csomagot telepítésre dupla kattintással!
 * A program kijelzi a függőségeket, melyeket szintén telepíteni kell: a Kijelölés gombra kattintással hagyja jóvá ezeket!
 * Az előző lépésekben csak kijelöltük telepítésre a csomagokat. A telepítés elvégzéséhez kattintson az eszköztár Alkalmaz gombjára!

A folyamat részleteit a Részletek feliratra kattintva megjelenítve ismét csak az apt parancs ismerős kimenete fogad bennünket. A grafikus felületen végrehajtott lépésekkel ekvivalens lett volna a sudo apt install apache2 parancs kiadása a terminálban. Bármelyik megoldást is választottuk, az telepítés eredményeként a gépünkre került az Apache 2 webszerver, amit a korábbi laborokban is használtunk - ha webes fejlesztési feladatunk lenne, most a saját gépünkön is tudnánk tesztelni pontosan olyan körülmények között, mint ahogy az a szerveren fut majd.

Nyisson egy Firefox böngészőt és nyissa meg a http://localhost oldalt, hogy a frissen telepített webszerver kezdőlapjáról képet tudjon készíteni!

![6. kérdés](06apache.png)

### 2.4. A fájlkezelő használata

Nyissa meg az asztalon található Saját mappa könyvtárat! Ezzel a hallgato felhasználó home könyvtára (a /home/hallgato mappa) tartalma jelenik meg a fájlkezelőben. 

Az eszköztár Ugrás a szülőmappára gombját (felfelé nyíl, az eszköztár harmadik gombja) duplán megnyomva a fájlrendszer gyökérkönyvtárába jutunk. 

A boot könyvtárba belépve a rendszer indulásához szükséges fájlokat látjuk: itt találhatók a telepített kernelek vmlinuz-* néven (több is lehet telepítve, de egyszerre természetesen csak egy verzió indul el a rendszer indulásakor), a bootolás során a kernel által használt kezdeti fájlrendszer (initrd.img*), a memtest86 alkalmazás (mellyel operációs rendszer betöltése nélkül tesztelhetjük a fizikai memóriát hibák után kutatva), illetve a grub könyvtár, ami az alapértelmezett Linux bootloader-t tartalmazza.

Készítsen egy képet a /boot könyvtár tartalmáról!

![7. kérdés](07boot.png)

## 3. A rendszer működésének mélyebb megértése

A Linux kernel kényelmesen elérhető felületet biztosít a programok felé: a Linux alapvető filozófiája szerint a rendszerben minden fájl, így a fájlrendszerbe ágyazott módon érhetünk férhetünk hozzá a legtöbb állapotváltozóhoz illetve adhatunk parancsokat a kernelnek. E megközelítés előnye, hogy tetszőlegesen egyszerű programok (például egyszerű szöveges shell scriptek) is hozzáférhetnek ezekhez a változókhoz.

Nyisson egy Terminál alkalmazást a rendszer menüből!

### 3.1. A kernel és a disztribúció verziója

A kernel verziójának meghatározásához adja ki az uname -a parancsot! A megjelenített kernel verzió valószínűleg nem a legfrissebb verzió amit a /boot könyvtárban látott: ha ugyanis az 1.4-es feladatban lefutott frissítés óta még nem indította újra a virtuális gépet, akkor a rendszer még a telepített és nem a legfrissebb kernellel fut.

Az éppen használt Linux disztribúció nevének és verziójának kiíratásához adja ki az lsb_release -a parancsot!

Készítsen képet az uname és az lsb_release parancsok kimenetéről!

![8. kérdés](08uname.png)

Ahogy korábban már említettük, a kernel az állapotváltozóit a fájlrendszer részeként jeleníti meg. A fájlrendszer gyökérkönyvtárában található /dev, a /proc és a /sys könyvtárak alatt található könyvtár hierarchia nem létezik a lemezen, ezeken a helyeken az elérési útvonalakon a kernel állapotváltozóit érhetjük el.

Adja ki a cat /proc/version parancsot! A kernel a fájlrendszerben ezen a ponton teszi közzé a verzióját, ebből dolgozik a korábban használt uname parancs is.

Adja ki a cat /proc/cmdline parancsot! Itt láthatja, hogy a grub a kernel betöltésekor milyen paramétereket adott át az induláshoz: a BOOT_IMAGE változóban szerepel a kernel fájl neve, a root paraméter a felcsatolandó fájlrendszer UUID-jét (Universally Unique IDentifier) tartalmazza, illetve néhány további paraméter is látható itt.

Készítsen képet a cat /proc/version és a cat /proc/cmdline két parancs kimenetéről!

![9. kérdés](09cmdline.png)

### 3.2. Memóriahasználat

Adja ki a `cat /proc/meminfo` parancsot! A kimenetben a memória és a swap állapotával kapcsolatos minden fontos állapotinformáció megtalálható. Mivel ezekre az esetek többségében nincs mind szükség, ezért a free nevet viselő program jobban formázva és csak a lényeget emeli ki (adja ki a free parancsot is)!

![10. kérdés](10mem.png)

A free parancs kimenetében feltűnhet, hogy nem csak egy számot kaptunk (a szabad memória méretét), hanem több paraméterünk is van, amik nem triviálisak. Nézzük meg ezeket alaposabban:

 * Total: a fizikai memória teljes mérete
 * Used: a folyamatok által lefoglalt és használt memória mennyisége
 * Free: szabad (semmire nem használt) memória
 * Shared: a folyamatok között megosztott memóriaterület
 * Buff/cache: a lemez gyorsítótárazására használt terület
 * Available: a szükség esetén felszabadítható memória mértéke

Könnyű belátni, hogy minden olyan memória, amit a rendszer semmire nem használ, a felhasználó szempontjából veszteség: hiszen ha a szabad memóriát bármilyen gyorsítótárazásra fel lehet használni, akkor érdemes is erre használni, mivel így a felhasználó számára a rendszer gyorsul anélkül, hogy bármelyik futó folyamat ennek kárát látná. Ezért van az, hogy a rendszer igyekszik a lemez gyorsítótárazásával (a legutóbb a lemezről beolvasott fájlok memóriában tartásával) gyorsítani a működést. Ha a programok memóriát igényelnek a kerneltől, akkor először a Free keretből kapnak, majd szükség esetén a buffereket felszabadítva az Available értékig adható további memória.

A Swap soron látható a lemezen elérhető memória cserehely mérete és kihasználtsága. Adja ki a `swapon --summary` parancsot, mellyel listázható a lemezen erre a célra allokált helyek listája: alapértelmezetten a rendszer jelenleg a /swapfile fájlt használja, de allokálhatnánk erre a célra további fájlokat és külön lemezpartíciókat is.

A kernel a régen használt memórialapokat hajlamos a lemezre menteni annak érdekében, hogy az éppen futó és újonnan induló programok számára mindig kellő mennyiségű szabad memória álljon rendelkezésre a gyors működés érdekében. Azt, hogy a kernel a memória hány százalékos kihasználtsága felett kezdi el használni a swap területet egy swappiness nevű kernel paraméter határozza meg. Értéke a `cat /proc/sys/vm/swappiness` parancs kiadásával kérhető le!

Készítsen képet a `cat /proc/sys/vm/swappiness` előző parancs kimenetéről!

![11. kérdés](11swappiness.png)

Az alapértelmezett 60-as érték azt jelenti, hogy amíg a fizikai memória 60%-a szabad, addig a kernel egyáltalán nem nyúl a lemezhez. Ha a szabad memória mértéke kevesebb mint 60%, akkor kezdi el a legrégebben használt lapokat a lemezre mozgatni.

### 3.3. Lemezkezelés

A rendszer által érzékelt lemezek listázásához adjuk ki a `sudo fdisk -l` parancsot! A rendszer a virtuális gépben beállított egyetlen lemezt `/dev/sda` néven ismeri fel, melyen három partíció található `/dev/sdaX` néven. 

A lemezek és a partíciók elnevezése nem véletlenül hasonlít a fájlnevek elérési újtára: a /dev (virtuális, a kernel által karbantartott) könyvtárban valóban létezik sda és sdaX nevű fájl, pontosabban device node, ami a fájlrendszeren keresztül elérhető így.

Az egyes partíciókon található fájlrendszerek helye a "/" fájlrendszer gyökér alatt attól függ, hogy azokat hova kötjük be a mount parancs segítségével. A `mount` parancsot kiadva láthatjuk az éppen felcsatolt összes fájlrendszert és azok csatolási helyét (mount point-ját)! A /dev/sda5 sort kikeresve látjuk, hogy az a fájlrendszer gyökérkönyvtára "/", a proc nevű virtuális fájlrendszer a /proc könyvtár alatt érhető el és így tovább.

A rendszer indulásakor felcsatolt fájlrendszerek a /etc/fstab fájlban találhatók, ennek tartalmát a `cat /etc/fstab` paranccsal írathatjuk ki. Mindössze két releváns sort látunk majd: a /dev/sda5 (mely itt UUID-jével szerepel) kerül felcsatolásara a "/" gyökér alá, illetve a /swapfile nem kerül sehova csatolásra ("none"), mert a típusa swap.

Készítsen képet a cat /etc/fstab kimenetéről!

![12. kérdés](12fstab.png)

Mivel a kernel számára nincs különbség a /dev alatti device node-ok és a fájlrendszer tetszőleges más pontjai között, ezért semmi akadálya, hogy egy partíciót vagy akár teljes lemezt fájlba másoljunk vagy onnan visszaállítsunk a sztenderd cp parancs segítségével (vagy a kicsit jobban paraméterezhető ezért jobb teljesítményű dd parancs használatával). Sőt, akár tetszőleges fájlt használhatunk fizikai lemez helyett, a rendszer számára ez se jelent különbséget. 

Ez utóbbi lehetőségre egy játékos példa:

 * Hozzunk létre egy üres, csak nullákat tartalmazó 10 MB méretű fájlt a következő parancs kiadásával: `dd if=/dev/zero of=/tmp/disk-file bs=1M count=10`
 * Ennek hatására létrejött egy 10 MB méretű disk-file névre hallgató fájl a /tmp könyvtárban, amit ellenőrizhetünk is: `ls -lh /tmp/disk-file`
 * Hozzunk létre EXT4 fájlrendszert a fájlon belül pont úgy, mintha egy lemezre mutató device node lenne: `mkfs.ext4 /tmp/disk-file`
 * A rendszer tartalmaz egy /mnt üres könyvtárat a fájlrendszer gyökerében. Csatoljuk fel az előbb létrehozott fájlrendszert oda: `sudo mount /tmp/disk-file /mnt`
 * A felcsatolt fájlrendszereket és az azokon elérhető szabad helyet a következő parancs jeleníti meg: `df -h`
   * Jól láthatjuk, hogy a /mnt alatt egy 5,7 MB-os kötet van felcsatolva, ahol még 5 MB szabad helyünk van. A hiányzó 4,3 MB helyet a fájlrendszer foglalja el, ami egy reális méretű lemez esetében arányaiban jóval kisebb veszteséget jelentene.

Készítsen egy képet a df -h kimenetéről a disk-file felcsatolása után!

![13. kérdés](13df.png)

### 3.4. A /dev fájlrendszer

A /dev könyvtár alatt találhatók a kernel által létrehozott device-node-ok. Szinte minden eszköz itt látható, ami a számítógéphez kapcsolódik, így a lemezek (pl. /dev/sda), a partíciók (pl. /dev/sda1), a fizikai terminálok (tty*), stb. Van itt néhány speciális elem is:

 * Az előbbi feladatban használtuk a disk-file csupa nullával való kitöltésére azt, hogy a /dev/zero-ból tetszőleges számú nulla érték kimásolható anélkül, hogy valaha is a fájl végére érnénk.
 * A /dev/random hasonlóan használható, csak nulla helyett pszeudovéletlen értékeket ad vissza, így például ha egy fájlt véglegesen és visszaállíthatatlanul törölni szeretnénk a lemezről, akkor először érdemes a /dev/random-ból származó értékekkel felülírni - így a tartalma jó eséllyel sose lesz már visszaállítható ellentétben az egyszerű törléssel, amikor a fájl tartalmának bájtjai a lemezen maradnak, csak a fájlrendszer rájuk mutató inode-ját töröljük.
 * A /dev/null egy végtelen nyelő, vagyis tetszőleges program kimenetét ideirányítva az nyom nélkül és a lemezen hely foglalás nélkül eltűnik, ami hasznos lehet ha például a konzolra feleslegesen író alkalmazás kimenetétől szeretnénk megszabadulni.

### 3.5 A /proc fájlrendszer

Minden futó folyamathoz részletes információk érhetők el a /proc virtuális fájlrendszer alatt. Próbaként indítsuk el a következő parancsot: `tail -f /var/log/syslog`

A tail parancs az -f kapcsolóval folyamatosan olvassa a paraméterként megadott rendszernapló fájlt, és amint egy új sor kerül annak a végére, megjeleníti azt. (A programból majd a Ctrl+C kombinációval tudunk kilépni, de egyelőre hagyjuk futni még!)

Mivel a futó tail program elfoglalja a meglevő terminálunkat indítson egy második terminál alkalmazást a rendszer menüből, és adja ki a következő parancsot: `ps axw | grep tail`

A ps parancs listázza az összes futó folyamatot, a kimenetét pedig a csővezetéken (pipe |) keresztül megkapja a grep, ami csak azokat a sorokat mutatja, melyek tartalmazzák a paraméterként kapott "tail" szót. A kimenetben keresse meg a "tail -f /var/log/syslog" végződésű sort, és az első oszlopból olvassa le a folyamat azonosítószámát (Process ID vagy röviden PID). A továbbiakban a parancsokon belül {PID} formában hivatkozunk majd az itt leolvasott számra, vagyis minden {PID} helyére helyettesítse majd be azt!

Először lépjünk át  megfelelő könyvtárba a `cd /proc/{PID}` parancs kiadásával! A rengeteg elérhető fájl közül néhány érdekesebb:

 * `cat limits`: a folyamatra érvényes korlátozások listázása (maximális CPU idő, egyszerre nyitottan tartható fájlleírók, stb.)
 * `cat status`: a folyamat részletes állapot információja (memória adatok, umask, swap használat, stb.)
 * `cat maps`: a folyamat által használt programkönyvtárak a megosztott memóriában
 * `ls -l root`: a folyamat által használt fájlrendszer gyökérkönyvtárára mutató szimbolikus link (jelen esetben ez a valós fájlrendszer gyökérkönyvtár a "/" lesz, de biztonsági megfontolásból egy folyamat bezárható egy tetszőleges mappába is)
 * `ls -l fd`: a folyamat által nyitott fájlleírók szimbolikus link formájában

Készítsen képet az ls -l fd kimenetéről!

![14. kérdés](14lsfd.png)

Egy pillanatra érdemes alaposabban szemügyre venni a nyitott fájl leírókat! Még a legegyszerűbb folyamat is legalább három nyitott fájl leíróval rendelkezik:

 * 0 = standard input - ez minden alkalmazás alapértelmezett konzolos bemenete, innen olvas a C gets() függvénye, a Java System.in objetuma vagy a Python sys.stdin objektuma,
 * 1 = standard output - ez minden alkalmazás alapértelmezett szöveges kimenete, ide ír a C printf() függvénye, a Java System.out objektuma vagy Pyhton sys.stdout objektuma,
 * 2 = standard error - erre a kimenetre küldik a programok a futás közbeni hibaüzeneteket, például a különböző kezeletlen kivételekről itt jelennek meg üzenetek.

A fentieken felül a futó tail programunk nyitva tartja még a /var/log/syslog fájlt 3-as sorszámmal (hiszen innen olvassa az új sorokat), és van egy 4-es sorszámú inode:inotify leírója is, melyen keresztül az operációs rendszertől kap jelzéseket arról, hogy új adat került a syslog fájl végére.

A /proc fájlrendszeren található adatokból dolgozik a pstree parancs, mely a folyamatokat leszármazás alapján fa struktúrában ábrázolja. Adja ki a `pstree | less` parancsot!

Készítsen képet a pstree | less kimenetéről!

![15. kérdés](15pstree.png)

A kirajzolt fa struktúrát a less program bemenetére irányítottuk a | karakter használatával, így a fel-le gombokkal mozoghat benne, kilépni pedig a q lenyomásával tud majd!

Jól látható, hogy minden folyamat őse a systemd: ez indul el először és ennek a PID-je az 1-es minden esetben. A grafikus környezet úgy indult el, hogy a systemd elindította a lightdm ablakkezelőt, ami elindította az Xorg névre hallgató X szervert, és megjelenítette a lightdm bejelentkező képernyőjét.

A sikeres belépés után indult el a cinnamon-session, ami a Cinnamon ablakkezelőt indította el, és ez az az asztali felület amit látunk magunk előtt bejelentkezés után. Minden itt látható program részletes adatai elérhetők a PID-jük kikeresése után a /proc fájlrendszeren.

Ezen a ponton a megfelelt teljesítéshez szükséges feladatok végére ért.
Amennyiben a laboron nem szeretne kiválóan megfelelt értékelést szerezni, akkor a további válasz mezőkbe egy-egy "-" karakter bejegyzésével jelezze, hogy a jegyzőkönyv értékelésével ne várjunk az extra feladatok megoldására!

## 4. Kísérletezés más Linux disztribúciókkal* (csak a kiválóan megfelelt értékeléshez)

A Linux laborok során már telepített konzolos Ubuntu és grafikux Linux Mint operációs rendszert. Ebben a feladatban Önre van bízva a választás, hogy egy további, grafikus felülettel rendelkező asztali Linux disztribúciót kipróbáljon! Ha már hallott egy disztribúcióról korábban, és szívesen kipróbálná azt, itt a lehetőség! Ha nincs ötlete a választáshoz, akkor a distrowatch.com (végtelenül retró megjelenésű) oldalon a bal szélen talál egy listát, ahol a keresések szám alapján népszerűségi sorrendben láthat 100 disztribúciót! Ha szeretne, kipróbálhat más nyílt forráskódú Unix variánst is, például a FreeBSD-t!

Hozzon létre egy új virtuális gépet, vagy törölje a meglevő virtuális gép virtuális merevlemezét, és telepítsen abba! Felhasználói névnek minden esetben a saját Neptun kódját használja, hogy a készített képernyőképeken az látható legyen!

Melyik disztribúciót választotta? Írja ide a nevét és verziószámát!

Alpine Linux 3.13

Adjon  2-3 mondatos magyar nyelvű leírást a választott operációs rendszerről! További 1 mondatban indokolja, hogy miért ezt választotta!

Az alpine az egyik legminimalistább distro, amiről hallottam, emellett nagyon biztonságos is, és kevés erőforrást igényel.
Érdekesség, hogy az alpine az nem GNU/Linux, vagyis nem GNU alapú userland-et használ, hanem musl alapút.

Készítsen egy képet a feltelepített rendszerről úgy, hogy látható legyen a felhasználói neve (=saját Neptun kódja) is a képen, például egy nyitott terminál ablakban!

![18. kérdés](16alpine.png)