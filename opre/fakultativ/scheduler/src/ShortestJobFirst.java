import java.util.ArrayList;
import java.util.List;

public class ShortestJobFirst {
    private final List<Task> queue = new ArrayList<>();
    
    public void addTask(Task task) {
        queue.add(task);
    }
    
    public void process() {
        List<Task> tasks = new ArrayList<>(queue);
        Task toRun = tasks.get(0);
        for (Task task : tasks) {
            if (task.isRun) {
                toRun = task;
                break;
            }
            if (task.strokeTime < toRun.strokeTime)
                toRun = task;
        }
        toRun.run();
        Main.ran.add(toRun);

        for (Task t: tasks) {
            if (t != toRun) {
                t.waitForRun();
            }
        }
        
        if (toRun.strokeTime <= 0) {
            queue.remove(toRun);
            for (Task t : Main.done) {
                if (t.name.equals(toRun.name)) {
                    t.waitTime = toRun.waitTime;
                }
            }
        }
    }
    
    public boolean isEmpty() {
        return queue.isEmpty();
    }
}