import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    
    public static List<Task> done = new ArrayList<>();
    public static List<Task> ran = new ArrayList<>();

    public static void main(String[] args) {
        int time = 0;
        List<Task> toBeRun = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        ShortestJobFirst sjf = new ShortestJobFirst();
        RoundRobin rr = new RoundRobin();

        while (scanner.hasNextLine()) {
            try {
                String taskString = scanner.nextLine();
                

                String[] taskArray = taskString.split(",");
                if (taskArray.length > 3) {
                    Task task = new Task(taskArray[0], Integer.parseInt(taskArray[1]), Integer.parseInt(taskArray[2]), Integer.parseInt(taskArray[3]));

                    toBeRun.add(task);
                }
                
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        
        done = new ArrayList<>(toBeRun);

        while (!toBeRun.isEmpty() || !sjf.isEmpty() || !rr.isEmpty()) {
            List<Task> tasks = new ArrayList<>(toBeRun);
            for (Task task : tasks) {
                if (task.startTime == time) {
                    if (task.priority > 0) {
                        sjf.addTask(task);
                    } else {
                        rr.addTask(task);
                    }
                    toBeRun.remove(task);
                }
            }
            if (!sjf.isEmpty()) {
                sjf.process();
                rr.waitAll();
            } else if (!rr.isEmpty()){
                rr.process();
            }
            time++;
        }
        
        Task lastPrinted = null;
        for (Task task : ran) {
            if (task != lastPrinted) {
                System.out.print(task.name);
                lastPrinted = task;
            }
        }
        System.out.println();

        int i = 0;
        for (Task task : done) {
            i++;
            System.out.print(task.name + ":" + task.waitTime);
            if (i < done.size()) {
                System.out.print(",");
            }
        }
        System.out.println();
    }
}
