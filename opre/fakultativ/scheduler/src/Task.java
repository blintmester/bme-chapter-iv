public class Task {
    public String name;
    public int priority;
    public int startTime;
    public int strokeTime;
    public int waitTime;
    public boolean isRun;
    
    public Task(String n, int prio, int startT, int strokeT) {
        name = n;
        priority = prio;
        startTime = startT;
        strokeTime = strokeT;
        waitTime = 0;
        isRun = false;
    }
    
    public void waitForRun() {
        waitTime++;
        isRun = false;
    }
    
    public void run() {
        isRun = true;
        strokeTime--;
    }
}