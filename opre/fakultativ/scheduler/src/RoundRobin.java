import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RoundRobin {
    private final List<Task> queue = new ArrayList<>();
    private int cycle = 0;

    public void addTask(Task task) {
        queue.add(task);
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
    
    public void process() {
        Task task = queue.get(0);
        if (cycle == 2) {
            queue.remove(task);
            queue.add(task);
            cycle = 0;
        }
        task = queue.get(0);
        task.run();
        Main.ran.add(task);
        cycle++;

        for (int i = 1; i < queue.size(); i++) {
            queue.get(i).waitForRun();
        }

        if (task.strokeTime <= 0) {
            for (Task t : Main.done) {
                if (t.name.equals(task.name)) {
                    t.waitTime = task.waitTime;
                }
            }
            queue.remove(task);
        }
    }
    
    public void waitAll() {
        for (Task t : queue) {
            t.waitForRun();
        }
    }

}