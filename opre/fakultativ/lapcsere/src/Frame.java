public class Frame {
    private int lastAccessed;
    private int page = Integer.MIN_VALUE;

    private int frozen = Integer.MIN_VALUE;
    private final String name;

    public Frame(String name) {
        this.name = name;
    }

    public int getFrozen() {
        return frozen;
    }

    public int getLastAccessed() {
        return lastAccessed;
    }

    public void load(int page, int t) {
        this.page = page;
        frozen = t;
        lastAccessed = t;
    }

    public boolean access(int page, int t) {
        if (page == this.page) {
            lastAccessed = t;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return name;
    }
}
