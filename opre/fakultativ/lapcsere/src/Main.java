import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static List<Frame> frames = new ArrayList<>();

    public static void main(String[] args) {
        frames.add(new Frame(Character.toString('A')));
        frames.add(new Frame(Character.toString('B')));
        frames.add(new Frame(Character.toString('C')));

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            try {
                String str = scanner.nextLine();
                String[] strs = str.split(",");

                int t = 0;
                int errors = 0;
                for (String s : strs) {
                    int page = Integer.parseInt(s);
                    page = Math.abs(page);
                    boolean loaded = false;

                    for (Frame f : frames) {
                        if (f.access(page, t)) {
                            loaded = true;
                            break;
                        }
                    }

                    if (loaded) {
                        System.out.print("-");
                    } else {
                        Frame lru = null;
                        for (Frame f : frames) {
                            if (f.getFrozen() >= t-3) {
                                continue;
                            }
                            if (lru == null || lru.getLastAccessed() > f.getLastAccessed()) {
                                lru = f;
                            }
                        }
                        errors++;
                        if (lru == null) {
                            System.out.print("*");
                        } else {
                            lru.load(page, t);
                            System.out.print(lru.toString());
                        }
                    }
                    t++;
                }

                System.out.println("");
                System.out.println(errors);

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

    }
}
