//import { Player } from './player.js';

export class Game {

    constructor() {
        this.player1 = new Player("1. játékos");
        this.player2 = new Player("2. játékos");

        this.dontetlen = 0;
        this.arany = 0;
    }

    setArany(newArany) {
        this.arany = newArany;
    }

    getArany() {
        return this.arany;
    }

    increaseDontetlen() {
        this.dontetlen++;
    }

    getDontetlen() {
        return this.dontetlen;
    }

    increaseWon(number) {
        switch (number) {
            case 1:
                this.player1.won();
                break;
            case 2:
                this.player2.won();
                break;
        }
    }

    getWons() {
        const one = this.player1.getWon();
        const two = this.player2.getWon();
        return { one, two };
    }

    getNames() {
        const one = this.player1.getName();
        const two = this.player2.getName();

        return { one, two };
    }

    setPlayerName(number, name) {
        switch (number) {
            case 1:
                this.player1.setName(name);
                break;
            case 2:
                this.player2.setName(name);
                break;
        }
    }
}