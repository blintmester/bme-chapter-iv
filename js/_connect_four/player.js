export class Player {
    constructor(name) {
        this.name = name;
        this.won = 0;
    }

    setName(newName) {
        this.name = newName;
    }

    getName() {
        return this.name;
    }

    increase() {
        this.won++;
    }

    getWon() {
        return this.won;
    }
}