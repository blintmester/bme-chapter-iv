# hatvanyozas

def paros(szam):
    mod = szam % 2 == 0
    return mod


def hatvanyozas(szam, hanyadikra):
    hatvany = 1
    if hanyadikra == 0:
        return hatvany
    else:
        if paros(hanyadikra):
            hatvany = hatvanyozas((szam*szam), (hanyadikra//2))
        else:
            hatvany = szam * hatvanyozas(szam, (hanyadikra - 1))

    return hatvany


print(hatvanyozas(2, 10))
