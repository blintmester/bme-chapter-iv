# lista legnagyobb eleme

def beolvas():
    return [int(elem) for elem in input().split()]


def legnagyobb(lista):
    hossz = len(lista)
    if hossz == 1:
        return lista[0]
    if hossz == 2:
        if lista[0] >= lista[1]:
            return lista[0]
        else:
            return lista[1]
    else:
        egyik = legnagyobb(lista[0:(hossz//2)])
        masik = legnagyobb(lista[(hossz//2):])

        if egyik >= masik:
            return egyik
        else:
            return masik


def kiir(n):
    print(n)


kiir(legnagyobb(beolvas()))
