# McCarthy fele 91-es függvény

def mcCarthy(szam):
    if szam > 100:
        return szam-10
    else:
        return mcCarthy(mcCarthy(szam+11))


def beolvas():
    return int(input())


def kiir(n):
    print(n)


kiir(mcCarthy(beolvas()))
