# szilveszteri filmek

class Ora:
    def __init__(self, ora=0, perc=0):
        self.ora = ora
        self.perc = perc

    def to_perc(self):
        return self.ora * 60 + self.perc


class Film:
    def __init__(self):
        self.kezdo = 0
        self.vege = 0
        self.ado = -1


def beolvasas():
    n = int(input())
    filmek = []
    for i in range(n):
        sor = input().split()
        film = Film()
        film.ado = sor[0]
        k = Ora()
        k.ora = int(sor[1])
        k.perc = int(sor[2])
        film.kezdo = k.to_perc()
        v = Ora()
        v.ora = int(sor[3])
        v.perc = int(sor[4])
        film.vege = v.to_perc()
        filmek.append(film)
    return filmek, n


def Rendezes(n, veg):
    S = [0] * n
    for i in range(n):
        S[i] = i
    for i in range(n):
        for j in range(i + 1, n):
            if veg[S[i]] > veg[S[j]]:
                cs = S[i]
                S[i] = S[j]
                S[j] = cs
    return S


def Kivalogatas(n, kezdo, veg):
    rendezett = Rendezes(n, veg)
    kivalogatott = [rendezett[0]]
    j = 0
    for i in range(1, n):
        if kezdo[rendezett[i]] >= veg[rendezett[j]]:
            kivalogatott.append(rendezett[i])
            j = i
    return len(kivalogatott), kivalogatott


def eltelt_ido(filmek, kivalogatott):
    eltelt = 0
    for i in range(len(filmek)):
        for j in kivalogatott:
            if i == j:
                eltelt += filmek[i].vege - filmek[i].kezdo

    return eltelt


filmek, n = beolvasas()
kezdo = []
veg = []
for f in filmek:
    kezdo.append(f.kezdo)
    veg.append(f.vege)

db, kivalogatott = Kivalogatas(n, kezdo, veg)
eltelt = eltelt_ido(filmek, kivalogatott)
print(db, eltelt)
for i in kivalogatott:
    print(i+1, end=" ")
