# vendegek fotozása

# Emberek (események) száma: N. Érkezési idők: Ei. Távozási idők: Ti. Eredeti (rendezés előtti) sor-szám: Si.
#
#
# Kiválogatás(N,E,T,Db,X):
#     Rendezés(N,E,T,S)
#     DB:=1; X(Db):=S(1); j:=1
#     Ciklus i=2-től N-ig
#         HaE(i)≥T(j) akkor Db:=Db+1; X(Db):=S(i); j:=i
#     Ciklus vége
#     Eljárás vége.

def beolvas():
    print("Hány vendég lesz?")
    vendegek_szama = int(input())
    erkezesek = []
    tavozasok = []

    for vendeg in range(vendegek_szama):
        # mivel nem volt specifikálva az érkezési és távozási idők formátuma,
        # így ezt nem óra perc formában kezeltem, hanem egy számként,
        # pl 3-kor érkezett és 12-kor távozott
        print("%d. vendég érkezési és távozási ideje (egysorban, szóközzel elválasztva):"%(vendeg + 1))
        sor = input().split()
        erkezesek.append(sor[0])
        tavozasok.append(sor[1])

    return vendegek_szama, erkezesek, tavozasok


def Rendezes(darab, veg):
    rendezett = [0] * darab
    for i in range(darab):
        rendezett[i] = i
    for i in range(darab):
        for j in range(i + 1, darab):
            if veg[rendezett[i]] > veg[rendezett[j]]:
                csere = rendezett[i]
                rendezett[i] = rendezett[j]
                rendezett[j] = csere
    return rendezett


def Kivalogatas(darab, kezdo, veg):
    rendezett = Rendezes(darab, veg)
    kivalogatott = [rendezett[0]]
    j = 1
    for i in range(1, darab):
        if kezdo[i] >= veg[j]:
            kivalogatott.append(rendezett[i])
            j = i
    return kivalogatott


def kiiras(kivalogatott):
    print("%d darab fotót kell készíteni."%len(kivalogatott))


# foprogram

vendegek, erkezesek, tavozasok = beolvas()
kiiras(Kivalogatas(vendegek, erkezesek, tavozasok))