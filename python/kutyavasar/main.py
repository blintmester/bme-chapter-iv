# kutyavásár

def beolvas():
    print("Hány vevő van?")
    vevok_szama = int(input())
    print("Hány kutya van?")
    kutyak_szama = int(input())

    ajanlatok = {}
    for kutya in range(kutyak_szama):
        vevok_ajanlata = []
        print("%d. kutyáért az ajánlatok:"%(kutya))
        for vevo in range(vevok_szama):
            vevok_ajanlata.append(int(input()))
        ajanlatok[kutya] = vevok_ajanlata

    return vevok_szama, kutyak_szama, ajanlatok


def feldolgozas(vevok, kutyak, ajanlatok):
    bevetel = 0
    for kutya in range(kutyak):
        max_ajanlat = -1
        for vevo in range(vevok):
            if ajanlatok[kutya][vevo] > max_ajanlat:
                max_ajanlat = vevo  # megadja, melyik vevo akarja megvenni
        bevetel += ajanlatok[kutya][max_ajanlat]
        print("%d. kutya %d-ért kelt el"%(kutya + 1, ajanlatok[kutya][max_ajanlat]))

    print("Az eladó össz bevétele: %d"%(bevetel))


# foprogram

vevok, kutyak, ajanlatok = beolvas()
feldolgozas(vevok, kutyak, ajanlatok)
