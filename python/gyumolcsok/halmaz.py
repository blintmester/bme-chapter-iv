class Halmaz:
    def __init__(self):
        self.__elemek = []

    # lehessen végigloopolni az elemeken
    @property
    def elemek(self):
        return self.__elemek
        
    # kényelmesebb méretmeghatározáshoz
    @property
    def meret(self):
        return len(self.__elemek)

    def Beolvas(self):
        print("Hány eleme van a halmaznak?")
        elemszam = int(input())
        for i in range(elemszam):
            hiba = True
            while hiba:
                print("Írd be a halmaz elemét!")
                elem = int(input())
                hiba = self.Eleme_e(elem)
                if hiba:
                    print("Ez az elem már benne van a halmazban")
                else:
                    self.__elemek.append(elem)

    def Kiiras(self):
        print("A halmaz elemszáma:", len(self.__elemek))
        for i in range(len(self.__elemek)):
            print(self.__elemek[i], end=" ")
        print()

    def Üres(self):
        self.__elemek.clear()

    def Ürese(self, elem):
        return len(self.__elemek)

    def Halmazba(self, elem):
        i = 0
        while i < len(self.__elemek) and self.__elemek[i] != elem:
            i += 1
        if i >= len(self.__elemek):
            self.__elemek.append(elem)

    def Halmazbol(self, elem):
        i = 0
        while i < len(self.__elemek) and self.__elemek[i] != elem:
            i += 1
        if i < len(self.__elemek):
            self.__elemek.pop(i)

    def Eleme_e(self, elem):
        i = 0
        while i < len(self.__elemek) and self.__elemek[i] != elem:
            i += 1
        return i < len(self.__elemek)

    def Resze(self, masikhalmaz):
        i = 0
        while i < len(self.__elemek) and masikhalmaz.Eleme_e(self.__elemek[i]):
            i += 1
        return i >= len(self.__elemek)

    def Unio(self, masikhalmaz):
        unioh = Halmaz()
        unioh.__elemek = self.__elemek.copy()
        for i in range(len(masikhalmaz.__elemek)):
            unioh.Halmazba(masikhalmaz.__elemek[i])
        return unioh

    def __add__(self, masikhalmaz):  # + operátor felüldefiniálása
        unioh = Halmaz()
        unioh.__elemek = self.__elemek.copy()
        for i in range(len(masikhalmaz.__elemek)):
            unioh.Halmazba(masikhalmaz.__elemek[i])
        return unioh

    def Metszet(self, masikhalmaz):
        metszeth = Halmaz()
        for i in range(len(self.__elemek)):
            if masikhalmaz.Eleme_e(self.__elemek[i]):
                metszeth.Halmazba(self.__elemek[i])
        return metszeth

    def __mul__(self, masikhalmaz):
        metszeth = Halmaz()
        for i in range(len(self.__elemek)):
            if masikhalmaz.Eleme_e(self.__elemek[i]):
                metszeth.Halmazba(self.__elemek[i])
        return metszeth

    def Rendezes(self):
        self.__elemek = sorted(self.__elemek)
        return self
