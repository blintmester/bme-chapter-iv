# gyümölcskereskedők


# az órai Halmaz osztályból indultam ki,
# csak módosítottam a feladaspecifikus részeket, valamint kiegészítettem,
# illetve töröltem metódusokat
class Halmaz:
    def __init__(self):
        self.__elemek = []

    # lehessen végigloopolni az elemeken
    @property
    def elemek(self):
        return self.__elemek

    def Beolvas(self):
        elemszam = int(input())
        for i in range(elemszam):
            elem = input()
            hiba = self.Eleme_e(elem)
            if not hiba:
                self.__elemek.append(elem)

    def Kiiras(self):
        print(len(self.__elemek))
        for i in range(len(self.__elemek)):
            print(self.__elemek[i])

    def Halmazba(self, elem):
        i = 0
        while i < len(self.__elemek) and self.__elemek[i] != elem:
            i += 1
        if i >= len(self.__elemek):
            self.__elemek.append(elem)

    def Halmazbol(self, elem):
        i = 0
        while i < len(self.__elemek) and self.__elemek[i] != elem:
            i += 1
        if i < len(self.__elemek):
            self.__elemek.pop(i)

    def Eleme_e(self, elem):
        i = 0
        while i < len(self.__elemek) and self.__elemek[i] != elem:
            i += 1
        return i < len(self.__elemek)

    def Unio(self, masikhalmaz):
        unioh = Halmaz()
        unioh.__elemek = self.__elemek.copy()
        for i in range(len(masikhalmaz.__elemek)):
            unioh.Halmazba(masikhalmaz.__elemek[i])
        return unioh

    def Metszet(self, masikhalmaz):
        metszeth = Halmaz()
        for i in range(len(self.__elemek)):
            if masikhalmaz.Eleme_e(self.__elemek[i]):
                metszeth.Halmazba(self.__elemek[i])
        return metszeth

    def Rendezes(self):
        self.__elemek = sorted(self.__elemek)
        return self


def beolvas():
    gyumi_elso = Halmaz()
    gyumi_masodik = Halmaz()

    gyumi_elso.Beolvas()
    gyumi_masodik.Beolvas()

    return gyumi_elso, gyumi_masodik


def feldolgozas(egyik, masik):
    unio = egyik.Unio(masik)
    metszet = egyik.Metszet(masik)

    for gyumi in metszet.elemek:
        unio.Halmazbol(gyumi)

    return unio.Rendezes()


def kiiras(unio):
    unio.Kiiras()


# foprogram

elso, masodik = beolvas()
kiiras(feldolgozas(elso, masodik))
