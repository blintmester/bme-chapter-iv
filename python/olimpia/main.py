# olimpiai láng

def beolvas():
    print("Két város távolsága, és a futók száma:")
    sor = input().split()
    tavlsag = int(sor[0])
    futok = int(sor[1])
    print("Hányadiktól hányadikig fut?")
    kiindulo = []
    erkezes = []
    for futo in range(futok):
        sor = input().split()
        kiindulo.append(int(sor[0]))
        erkezes.append(int(sor[1]))

    return tavlsag, futok, kiindulo, erkezes


def rendezes(darab, veg):
    rendezett = [0] * darab
    for i in range(darab):
        rendezett[i] = i
    for i in range(darab):
        for j in range(i + 1, darab):
            if veg[rendezett[i]] > veg[rendezett[j]]:
                csere = rendezett[i]
                rendezett[i] = rendezett[j]
                rendezett[j] = csere
    return rendezett


def stafeta(futok_szama, kezdo, veg):
    rendezett = rendezes(futok_szama, kezdo)
    kivalogatott = [rendezett[0]]
    legmesszebb = 0
    for i in range(1, futok_szama - 1):
        if veg[rendezett[i]] > veg[rendezett[legmesszebb]]:
            legmesszebb = i
        if kezdo[rendezett[i + 1]] >= veg[kivalogatott[len(kivalogatott) - 1]]:
            kivalogatott.append(rendezett[legmesszebb])
    return kivalogatott


def kiiras(futok):
    print(len(futok))
    for futo in futok:
        print(futo + 1, end=" ")
    print()


# foprogram

tavlsag, futok, kiindulo, erkezes = beolvas()
kiiras(stafeta(futok, kiindulo, erkezes))
