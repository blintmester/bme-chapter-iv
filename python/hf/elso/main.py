# 9. feladat

# A meteorológiai intézet az ország valahány településére adott valamennyi napos időjárás előrejelzést
# (nem tudjuk előre, hány településre és hány napra), az adott településen az adott napra várt legmagasabb hőmérsékletet.
# Készíts programot, amely megadja a legszélsőségesebb településeket (települések sorszámai),
# azaz azokat, ahol a legkisebb és a legnagyobb várt hőmérséklet eltérése maximális!

def be_fajl_nyit():
    hiba = True
    while hiba:
        try:
            print("Add meg a bemeneti fájlnevet!")
            fajl_nev = input()
            be_fajl = open(fajl_nev, "rt")
            hiba = False
        except:
            print("Nem sikerült")

    print("A bemeneti fájl (", fajl_nev, ") sikeresen megnyílt")
    return be_fajl


def ki_fajl_nyit():
    hiba = True
    while hiba:
        try:
            print("Add meg a kimeneti fájlnevet!")
            fajl_nev = input()
            ki_fajl = open(fajl_nev, "wt")
            hiba = False
        except:
            print("Nem sikerült")

    print("A kimeneti fájl (", fajl_nev, ") sikeresen létrejött")
    return ki_fajl


def feldolgozas(fajl):
    legnagyobb_elteres = 0
    szelsoseges_telepulesek = []
    sor = fajl.readline().strip()
    i = 1
    while sor != "":
        elt = elteres(sor)
        if elt == legnagyobb_elteres:
            szelsoseges_telepulesek.append(i)
        if elt > legnagyobb_elteres:
            legnagyobb_elteres = elt
            szelsoseges_telepulesek = [i]
        i += 1
        sor = fajl.readline().strip()
    fajl.close()
    return szelsoseges_telepulesek


def elteres(sor):
    sorlista = sor.split()
    legkisebb = 100  # nyugodtan választhatjuk végtelennek a 100-at, hőmérsékletekről van szó
    legnagyobb = -100
    for i in sorlista:
        if int(i) < legkisebb:
            legkisebb = int(i)
        if int(i) > legnagyobb:
            legnagyobb = int(i)
    return legnagyobb - legkisebb


def kiir_fajlba(fajl, lista):
    for li in lista:
        fajl.write(str(li) + "\n")
    fajl.close()


# főprogram

be_fajl = be_fajl_nyit()
ki_fajl = ki_fajl_nyit()

kiir_fajlba(ki_fajl, feldolgozas(be_fajl))
