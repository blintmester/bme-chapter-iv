# tanulok jegyei

def open_file_read(filename):
    error = True

    while error:
        try:
            file = open(filename, "rt", encoding="utf-8")
            error = False
            return file
        except:
            print("Nem létező fájlnevet adtál meg!")  # remélhetőleg ide nem jutunk el


def open_file_write(filename):
    error = True

    while error:
        try:
            file = open(filename, "wt", encoding="utf-8")
            error = False
            return file
        except:
            print("Nem létező fájlnevet adtál meg!")  # remélhetőleg ide nem jutunk el


def get_student_neptun():
    error = True

    while error:
        try:
            print("Adj meg egy neptun kódot!")
            neptun = input()
            error = False
            return neptun
        except:
            print("Valami nem OK")


def find_student(neptun, rfile, wfile):
    line = rfile.readline().strip()  # eldobjuk az első sort
    line = rfile.readline().strip()  # beolvassuk az első valódi adatsort
    separator = ";"

    lst = line.split(separator)  # feldaraboljuk a ; mentén, így megkapjuk egy sor elemeit

    data = {}

    data = {
        "id": lst[0],
        "last_name": lst[1],
        "first_name": lst[2],
        "city": lst[3],
        "address": lst[4],
    }

    while line != "" and data["id"] != neptun:
        data = {}
        lst = line.split(separator)  # feldaraboljuk a ; mentén, így megkapjuk egy sor elemeit

        data = {
            "id": lst[0],
            "last_name": lst[1],
            "first_name": lst[2],
            "city": lst[3],
            "address": lst[4],
        }

        line = rfile.readline().strip()

    ok = data["id"] == neptun

    if ok:
        print_student_data(data, wfile)  # TODO: print to file
    else:
        print("Nem található ilyen neptun-kódú tanuló")


def print_student_data(data, file):  # szerintem ez magáért beszél
    print("A tanuló neptun kódja:", data["id"], ", neve:", data["last_name"], data["first_name"],
          ", és címe:", data["city"], data["address"])

    file.write("A tanuló neptun kódja: " + data["id"] + ", neve: " + data["last_name"] + " " +  data["first_name"] +
               ", és címe: " + data["city"] + " " + data["address"] + "\n")
    return


def find_and_print_student_notes_and_average(neptun, rfile, wfile):
    line = rfile.readline().strip()  # eldobjuk az első sort
    line = rfile.readline().strip()  # beolvassuk az első valódi adatsort
    separator = ";"

    counter = 0  # számláló, darabszám
    total = 0  # összeg

    print("A tanuló jegyei: ", end=" ")  # end=" " nem írja új sorba
    wfile.write("A tanuló jegyei: ")

    while line != "":
        lst = line.split(separator)  # feldaraboljuk a ; mentén, így megkapjuk egy sor elemeit

        if lst[0] == neptun:
            print(lst[1], end=" ")  # kiírunk egy jegyet
            wfile.write(lst[1] + " ")  # kiírunk egy jegyet
            counter += 1  # növeljük a számlálónkat
            total += int(lst[1])  # növeljük az összeget

        line = rfile.readline().strip()

    if counter != 0:
        print("\nA tanuló jegyeinek átlaga: ", total / counter, "\n")
        wfile.write("\nA tanuló jegyeinek átlaga: " + str(total / counter) + "\n")
    else:
        print("Nincsen ilyen Neptun kódú tanuló")
        wfile.write("Nincsen ilyen Neptun kódú tanuló")


#######################################################################
#
# Most kezdődik a program, eddig csak függvényeket írtunk.
#
#######################################################################


notes = open_file_read("Jegyek.csv")  # megnyitjuk a jegyek.csv-t olvasásra
students = open_file_read("Tanulok.csv")  # megnyitjuk a tanulok.csv-t olvasásra
file_to_write = open_file_write("ki.txt") # megnyitjuk a ki.txt-t írásra

neptun = get_student_neptun()
find_student(neptun, students, file_to_write)  # megkeressünk egy adott tanulót
find_and_print_student_notes_and_average(neptun, notes, file_to_write)  # megkeressük és kiírjuk a tanuló összes jegyét, majd az átlagát

# és persze NE felejtsük el bezárni a fájlokat, amiket megnyitottunk
notes.close()
students.close()
file_to_write.close()