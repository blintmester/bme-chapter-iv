import queue


class Graf:
    def __init__(self, ir=False):
        self.ellista = {}
        self.ir = ir
        self.szin = {}

    def BeszurPont(self, p):
        if not p in self.ellista.keys():
            self.ellista[p] = []
            self.szin[p] = "feher"

    def BeszurEl(self, p1, p2):
        if p1 != p2:
            if p1 in self.ellista.keys() and p2 in self.ellista.keys():
                if not p2 in self.ellista[p1]:
                    self.ellista[p1].append(p2)
                    if not self.ir:
                        self.ellista[p2].append(p1)

    def kiiras(self):
        for p in self.ellista.keys():
            print(p, end=" ")
            for kp in self.ellista[p]:
                print(kp, end=" ")
            print()

    def Feherrevalt(self):
        for p in self.ellista.keys():
            self.szin[p] = "feher"

    def SzelessegiBejaras(self, p):
        marbejart = []
        sorerintett = queue.Queue()
        sorerintett.put(p)
        self.szin[p] = "szurke"
        while sorerintett.qsize() > 0:
            p = sorerintett.get()
            self.szin[p] = "fekete"
            marbejart.append(p)
            for szp in self.ellista[p]:
                if self.szin[szp] == "feher":
                    sorerintett.put(szp)
                    self.szin[szp] = "szurke"
        return marbejart

    def SzelessegiBejarasModosit(self, p, K):
        marbejart = []
        sorerintett = queue.Queue()
        sorerintett.put((p, 1))  # hányadik hullámfront
        self.szin[p] = "szurke"
        while sorerintett.qsize() > 0:
            elem = sorerintett.get()
            p = elem[0]
            szam = elem[1]
            self.szin[p] = "fekete"
            marbejart.append(p)
            for szp in self.ellista[p]:
                if self.szin[szp] == "feher" and szam <= K:
                    sorerintett.put((szp, szam + 1))
                    self.szin[szp] = "szurke"
        return marbejart


# ha standard inputrol kene beolvasni
def beolvas():
    elso_sor = input().split()
    varos_szam = elso_sor[0]
    k = int(elso_sor[1])
    m = int(elso_sor[2])

    graf = Graf()
    for elem in range(m):
        sor = input().split()
        p1 = sor[0]
        p2 = sor[1]
        graf.BeszurPont(p1)
        graf.BeszurPont(p2)
        graf.BeszurEl(p1, p2)

    sor = input().split()
    kiindulo_pontok = []
    for v in range(len(sor)):
        kiindulo_pontok.append(sor[v])

    return graf, kiindulo_pontok, varos_szam, k


def nem_juthat_el(graf, kiindulo_pontok, K):
    osszes = []
    for kp in kiindulo_pontok:
        graf.Feherrevalt()
        marbejart = graf.SzelessegiBejarasModosit(kp, K)
        osszes = osszes + marbejart

    eselytelen = []
    for p in graf.ellista.keys():
        if not p in osszes:
            eselytelen.append(p)

    return eselytelen


# foprogram
graf, kiindulo_pontok, varos_szam, K = beolvas()
es = nem_juthat_el(graf, kiindulo_pontok, K)
for p in es:
    print(p, end=" ")
print()
