class TBinfa:
    def __init__(self):
        self.gyoker = None

    def Ures(self):
        self.gyoker = None

    def Urese(self):
        return self.gyoker == None

    def Egyelemufa(self, ertek):
        self.gyoker = ertek
        self.bal = None
        self.jobb = None

    def BalraIlleszt(self, balfa):
        # ellenorizni kellene, hogy van-e mar valami
        self.bal = balfa

    def JobbraIlleszt(self, jobbfa):
        self.jobb = jobbfa

    def BalGyerek(self):
        return self.bal

    def JobbGyerek(self):
        return self.jobb

    def Gyokerelem(self):
        return self.gyoker

    def Gyokermodosit(self, ertek):
        self.gyoker = ertek

    def BKJ(self):
        if not TBinfa.Urese(self):
            if self.BalGyerek() != None:
                self.BalGyerek().BKJ()
            print(TBinfa.Gyokerelem(self))
            if self.JobbGyerek() != None:
                self.JobbGyerek().BKJ()

    ################################################################
    # ez volt a hazi

    def BJK(self):
        if not TBinfa.Urese(self):
            if self.BalGyerek() != None:
                self.BalGyerek().BKJ()
            if self.JobbGyerek() != None:
                self.JobbGyerek().BKJ()
            print(TBinfa.Gyokerelem(self))

    def KBJ(self):
        if not TBinfa.Urese(self):
            print(TBinfa.Gyokerelem(self))
            if self.BalGyerek() != None:
                self.BalGyerek().BKJ()
            if self.JobbGyerek() != None:
                self.JobbGyerek().BKJ()

    ################################################################

    # ------------------------------------
    def Leveleleme(self):
        return self.bal == None and self.jobb == None

    def Hanyelemu(self):
        if self.gyoker == None:
            return 0
        else:
            if self.bal == None and self.jobb == None:
                return 1
            else:
                if self.bal == None and self.jobb != None:
                    return 1 + self.jobb.Hanyelemu()
                else:
                    if self.jobb == None and self.bal != None:
                        return 1 + self.bal.Hanyelemu()
                    else:
                        return 1 + self.bal.Hanyelemu() + self.jobb.Hanyelemu()

    def Melyseg(self):
        if self.gyoker == None:
            return 0
        else:
            if self.bal == None and self.jobb == None:
                return 1
            else:
                return 1 + max(self.bal.Melyseg(), self.jobb.Melyseg())

    def Levelekszama(self):
        if self.gyoker == None:
            return 0
        else:
            if self.Leveleleme():
                return 1
            else:
                return self.bal.Levelekszama() + self.jobb.Levelekszama()
