# bankszamla

class BankSzamla:
    __SZAMOK_SZAMLALOJA = 0  # ez egy statikus (azaz konstans) változó

    def __init__(self, tulajdonos="", szamlaszam="", osszeg=0):  # konstruktor metódus, paraméterrel
        self._tulajdonos = tulajdonos
        self._szamlaszam = szamlaszam
        self._osszeg = int(osszeg)
        BankSzamla.__SZAMOK_SZAMLALOJA += 1  # növelem az összes létező bankszámla számát

    # készítünk neki egy pár getter-setter párost
    @property
    def tulajdonos(self):
        return self._tulajdonos

    @tulajdonos.setter
    def tulajdonos(self, value):
        self._tulajdonos = value

    @property
    def szamlaszam(self):
        return self._szamlaszam

    @property
    def osszeg(self):
        return self._osszeg

    @osszeg.setter
    def osszeg(self, value):
        self._osszeg = value

    # jóváírás függvény, egy összeget kér be
    def jovairas(self, osszeg=0):
        # ellenőrizzük, hogy pozitív számot szeretne-e feltölteni
        if osszeg <= 0:
            print("Csak pozitív értéket lehet jóváírni.")
        else:
            self.osszeg += osszeg

    # utalás függvény, bekér egy cél számlaszámot, és egy elküldendő összeget
    def utalas(self, hova="", osszeg=0):
        # ellenőrizzük, hogy pozitív számot szeretne-e feltölteni
        if osszeg <= 0:
            print("Csak pozitív értéket lehet jóváírni.")
        else:
            # ellenőrizzük, hogy a célszámla első 5 karaktere megegyezik-e a saját számlánk első 5 karakteréről
            if hova[0:5] != self.szamlaszam[0:5]:
                jutalek = osszeg / 1000 * 3  # kiszámoljuk az összeg 3 ezrelékét
                if jutalek < 300:
                    jutalek = 300
                self.osszeg -= jutalek

            # ha kell, ha nem kell jutalékot fizetni, akkor is mindenképpen levonjuk az összeget a számlánkról
            self.osszeg -= osszeg

    # számla infó kiírós függvény
    def kiir(self):
        print("számla tulajdonosa: " + self.tulajdonos + "\n" +
              "számla száma: " + self.szamlaszam + "\n" +
              "számlán lévő összeg: " + str(self.osszeg))


################################################################################
#
#
# az osztály tesztelése itt kezdődik, eddig nem hívtunk meg egy függvényt se
#
#
################################################################################

egyik = BankSzamla("egyik", "11111-22222-33333-444444", 1000)
masik = BankSzamla("masik", "11111-22222-33333-666666", 5000)
harmadik = BankSzamla("harmadik", "99999-888888-77777-66666", 500000)
egyik.kiir()
masik.kiir()
harmadik.kiir()

egyik.jovairas(2000)
egyik.kiir()

egyik.utalas(masik.szamlaszam, 1000)  # egyik.send_money("11111-22222-33333-666666", 1000)
egyik.kiir()

harmadik.utalas(egyik.szamlaszam, 200000)  # harmadik.send_money("11111-22222-33333-444444", 200000)
harmadik.kiir()
