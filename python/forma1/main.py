# Nagy Anna Fruzsina G76NQF
# Forma1 ZH

class Forma1:
    def __init__(self):
        self.__rajtszam = 0
        self.__nev = ""
        self.__koridok = []
        self.__teljesitett_korok = 0
        self.__kiesett_e = False

    @property
    def rajtszam(self):
        return self.__rajtszam

    @rajtszam.setter
    def rajtszam(self, uj_rajtszam):
        self.__rajtszam = uj_rajtszam

    @property
    def nev(self):
        return self.__nev

    @nev.setter
    def nev(self, uj_nev):
        self.__nev = uj_nev

    @property
    def koridok(self):
        return self.__koridok

    @koridok.setter
    def koridok(self, uj_koridok):
        self.__koridok = uj_koridok

    @property
    def teljesitett_korok(self):
        return self.__teljesitett_korok

    @teljesitett_korok.setter
    def teljesitett_korok(self, uj):
        self.__teljesitett_korok = uj

    @property
    def kiesett_e(self):
        return self.__kiesett_e

    @kiesett_e.setter
    def kiesett_e(self, uj):
        self.__kiesett_e = uj


def befilenyit():
    hiba = True
    while hiba:
        try:
            print("Írd be a bemeneti fájl nevét!")
            filenév = "forma1.csv"  # input()
            befile = open(filenév, "rt", encoding="utf-8")
            hiba = False
        except:
            print("Nem létező fájlnevet adtál meg!")
    print("A bemeneti fájl (", filenév, ") sikeresen megnyílt")
    return befile


def csv_fileolvas_listába(befile):
    sor = befile.readline().strip()  # Fejsor ki
    sor = befile.readline().strip()  # Első valódi adat
    cellahatároló = ";"
    lista = []
    while sor != "":
        sorlista = sor.split(cellahatároló)
        adat = Forma1()
        adat.nev = sorlista[0]
        adat.rajtszam = int(sorlista[1])

        koridok = []
        teljesitett_korok = 0
        for k in range(0, 9):
            koridok.append(int(sorlista[k + 2]))
            if koridok[k] != 0:
                teljesitett_korok = k + 1
        kiesett_e = teljesitett_korok == 10
        adat.koridok = koridok
        adat.teljesitett_korok = teljesitett_korok
        adat.kiesett_e = kiesett_e
        lista.append(adat)
        sor = befile.readline().strip()
    befile.close()
    return lista


def korido_osszeg(versenyzo):
    osszeg = 0
    for ido in versenyzo.koridok:
        osszeg += ido
    return osszeg


def kiesett_korben(versenyzo):
    kiesett_kor = 1
    for ido in versenyzo.koridok:
        if ido == 0:
            return kiesett_kor
        kiesett_kor += 1

    return 11


def kiiras(lista):
    print("A verseny végeredménye:")
    rendezett = sorted(lista, key=lambda versenyzo: (-kiesett_korben(versenyzo), korido_osszeg(versenyzo)))
    utolso_korido = -1
    for versenyzo in rendezett:
        print(versenyzo.nev, end=" ")
        print(versenyzo.rajtszam, end="\t")
        if kiesett_korben(versenyzo) != 11:
            print("DNF", end="")
        else:
            print(korido_osszeg(versenyzo), end="\t")
            if utolso_korido >= 0:
                print(utolso_korido - korido_osszeg(versenyzo), end="")
            utolso_korido = korido_osszeg(versenyzo)
        print()


def leggyorsabb(lista):
    return min(lista, key=lambda versenyzo: (-kiesett_korben(versenyzo), korido_osszeg(versenyzo))).nev


def kiir_leggyorsabb(lista):
    print("A leggyorsabb kört elért versenyző:")
    print(leggyorsabb(lista))


def elso_korben_kiesett_e(lista):
    for versenyzo in lista:
        if kiesett_korben(versenyzo) == 1:
            return True
    return False


def kiir_elso_korben_kieesett_e(lista):
    if elso_korben_kiesett_e(lista):
        print("Igen, volt olyan versenyző, aki az első körben kiesett.")
    else:
        print("Nem, nem volt olyan versenyző, aki az első körben kiesett.")


# foprogram

befile = befilenyit()
lista = csv_fileolvas_listába(befile)
kiiras(lista)
kiir_leggyorsabb(lista)
kiir_elso_korben_kieesett_e(lista)