# Nagy Anna Fruzsina G76NQF
# Forma1 ZH

class forma1:
    def __init__(self):
        self.__rajtszam = 0
        self.__nev = ""
        self.__koridok = 0
        self.__teljesitett_korok = 0
        self.__kiesett_e = False

    @property
    def rajtszam(self):
        return self.__rajtszam

    @rajtszam.setter
    def rajtszam(self, uj_rajtszam):
        self.__rajtszam = uj_rajtszam

    @property
    def nev(self):
        return self.__nev

    @nev.setter
    def nev(self, uj_nev):
        self.__nev = uj_nev

    @property
    def koridok(self):
        return self.__koridok

    @koridok.setter
    def koridok(self, uj_koridok):
        self.__koridok = uj_koridok

    @property
    def teljesitett_korok(self):
        return self.__teljesitett_korok

    @teljesitett_korok.setter
    def teljesitett_korok(self, uj):
        self.__teljesitett_korok = uj

    @property
    def kiesett_e(self):
        return self.__kiesett_e

    @kiesett_e.setter
    def kiesett_e(self, uj):
        self.__kiesett_e = uj


