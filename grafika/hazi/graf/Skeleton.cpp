//=============================================================================================
// Mintaprogram: Z�ld h�romsz�g. Ervenyes 2019. osztol.
//
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat, BOM kihuzando.
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni a printf-et kiveve
// - Mashonnan atvett programresszleteket forrasmegjeloles nelkul felhasznalni es
// - felesleges programsorokat a beadott programban hagyni!!!!!!! 
// - felesleges kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan OpenGL fuggvenyek hasznalhatok, amelyek az oran a feladatkiadasig elhangzottak 
// A keretben nem szereplo GLUT fuggvenyek tiltottak.
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : R�thelyi B�lint
// Neptun : IZUM0B
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem.
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem,
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================
#include "framework.h"

// vertex shader in GLSL: It is a Raw string (C++11) since it contains new line characters
const char * const vertexSource = R"(
	#version 330				// Shader 3.3
	precision highp float;		// normal floats, makes no difference on desktop computers

	uniform mat4 MVP;			// uniform variable, the Model-View-Projection transformation matrix
	layout(location = 0) in vec2 vp;	// Varying input: vp = vertex position is expected in attrib array 0

	void main() {
		gl_Position = vec4(vp.x, vp.y, 0, 1) * MVP;		// transform vp from modeling space to normalized device space
	}
)";

// fragment shader in GLSL
const char * const fragmentSource = R"(
	#version 330			// Shader 3.3
	precision highp float;	// normal floats, makes no difference on desktop computers
	
	uniform vec3 color;		// uniform variable, the color of the primitive
	out vec4 outColor;		// computed color of the current pixel

	void main() {
		outColor = vec4(color, 1);	// computed color is the color of the primitive
	}
)";

static float lorentz(const vec3 &a, const vec3 &b);
static float hyper_distance(const vec3 &a, const vec3 &b);
static vec3 hyper_vector(const vec3 &p, const vec3 &q);
static vec3 hyper_combination(const vec3 &p, const vec3 &q, float t);
static vec3 hyper_move(const vec3 &p, const vec3 &v, float d);
static vec3 hyper_mirror(const vec3 &p, const vec3 &m);

/**
 * Gr�f oszt�ly
 */
class Graph {
private:
	int count; // h�ny pontb�l �lljon a gr�f
	int line_count;

	/**
	 * pont oszt�ly
	 */
	class Point {
	public:
		vec3 coordinates;
		std::vector<Point*> neighbours; // szomsz�dokat pointerek seg�ts�g�vel t�rolom
		Point(vec3 coords = (0, 0, 0)) { coordinates.x = coords.x; coordinates.y = coords.y; coordinates.z = coords.z; }
	};

	std::vector<Point> points;

	void genPoints() {
		for (int i = 0; i < count; i++) {
			float x = -1.0 + 2.0 * (float(rand()) / float(RAND_MAX));
			float y = -1.0 + 2.0 * (float(rand()) / float(RAND_MAX));
			Point point;
			point.coordinates.z = sqrt(1 + x*x + y*y); // kisz�molom a hiperbolikus w koordin�t
			point.coordinates.x = x;
			point.coordinates.y = y;
			points.emplace_back(point);
		}
	}

	void genLines() {  //(50*49)/2 * 0,05 = 61,25

		// kisz�molom, h�ny �lnek kell lennie
		line_count = floor((count * (count - 1.0)) / 2.0 * 0.05);
		int lines = line_count;
		int i = 0;
		while(lines > 0) {
			if (i >= points.size()) { // ha t�lindexeln�k, kezdem el�lr�l
				i = 0;
			}
			Point *tmp;
			tmp = &points[rand() % points.size()];
			if (tmp == &points[i]) {
				continue; // ha �nmaga, gener�lok �jat
			}
			points[i].neighbours.emplace_back(tmp);
			tmp->neighbours.emplace_back(&points[i]);
			i++;
			lines--;
		}
	}

public:

	Graph(float cnt = 50) {
		//srand (time(NULL));
		count = cnt;
		genPoints();
		genLines();
	}

	float * getPoints() {
		float *p = new float[pointsSize()];
		int j = 0;

		// kisz�molja �s bet�lti egy t�mbbe a levetitett koordin�t�kat
		for (int i = 0; i < points.size(); i++) {
			p[j++] = points[i].coordinates.x / points[i].coordinates.z;
			p[j++] = points[i].coordinates.y / points[i].coordinates.z;
		}
		return p;
	}

	float * getLinesCoordinates() {
		float *l = new float[linesSize()];
		int k = 0;
		// kisz�molja �s bet�lti egy t�mbbe a levet�tett koordin�t�kat, k�tszer h�zok meg egy vonalat
		for (int i = 0; i < points.size(); i++) {
			for (int j = 0; j < points[i].neighbours.size(); j++) {
				l[k++] = points[i].coordinates.x / points[i].coordinates.z;
				l[k++] = points[i].coordinates.y / points[i].coordinates.z;
				l[k++] = points[i].neighbours[j]->coordinates.x / points[i].neighbours[j]->coordinates.z;
				l[k++] = points[i].neighbours[j]->coordinates.y / points[i].neighbours[j]->coordinates.z;
			}
		}
		return l;
	}

	// egy pontnak k�t koordin�t�j�t t�rolom
	int pointsSize() {
		return points.size()*2;
	}

	// egy vonalhoz k�t pont k�t koordin�t�ja tartozik, �s k�t vonal van mindig
	int linesSize() {
		return line_count*2*2*2;
	}

	void organize() {
		// TODO
	}

	void shift(const vec3 &p, const vec3 &q) {

		// keresek k�t pontot, a p �s q k�z�tti t�vols�g negyed�n �s h�romnegyed�n
		auto m1 = hyper_combination(p, q, 0.25f);
		auto m2 = hyper_combination(p, q, 0.75f);

		// minden pontot t�kr�z�k k�tszer
		for (int i = 0; i < points.size(); i++) {
			Point& point = points[i];
			point.coordinates = hyper_mirror(point.coordinates, m1);
			point.coordinates = hyper_mirror(point.coordinates, m2);
		}
	}
};

GPUProgram gpuProgram; // vertex and fragment shaders
unsigned int points_vao;	   // virtual world on the GPU
unsigned int lines_vao;	   // virtual world on the GPU
unsigned int points_vbo;		// vertex buffer object
unsigned int lines_vbo;		// vertex buffer object
Graph graph;
vec3 prev_mouse_pos;

/**
 * bem�solom az adatoakt a GPU ram-j�ba
 */
void pointsToVRam() {

	glBindVertexArray(points_vao);		// make it active
	glBindBuffer(GL_ARRAY_BUFFER, points_vbo);

	float *vertices = graph.getPoints();
	glBufferData(GL_ARRAY_BUFFER, 	// Copy to GPU target
				 graph.pointsSize()*sizeof(float),  // # bytes
				 vertices,	      	// address
				 GL_STATIC_DRAW);	// we do not change later

	glEnableVertexAttribArray(0);  // AttribArray 0
	glVertexAttribPointer(0,       // points_vbo -> AttribArray 0
						  2, GL_FLOAT, GL_FALSE, // two floats/attrib, not fixed-point
						  0, NULL); 		     // stride, offset: tightly packed

	delete[] vertices;
	/***********************************************************************/

	glBindVertexArray(lines_vao);		// make it active
	glBindBuffer(GL_ARRAY_BUFFER, lines_vbo);

	float *vertices2 = graph.getLinesCoordinates();
	glBufferData(GL_ARRAY_BUFFER, 	// Copy to GPU target
				 graph.linesSize()*sizeof(float),  // # bytes
				 vertices2,	      	// address
				 GL_STATIC_DRAW);	// we do not change later



	glEnableVertexAttribArray(0);  // AttribArray 0
	glVertexAttribPointer(0,      // lines_vbo -> AttribArray 1
						  2, GL_FLOAT, GL_FALSE, // two floats/attrib, not fixed-point
						  0, NULL); 		     // stride, offset: tightly packed
	delete[] vertices2;
}

/**
 * matematika k�pletek implement�l�sa
 */
static float lorentz(const vec3 &a, const vec3 &b) {
	return (a.x * b.x + a.y * b.y - (a.z * b.z));
}

static float hyper_distance(const vec3 &a, const vec3 &b) {
	return acoshf(-lorentz(a, b));
}

static vec3 hyper_vector(const vec3 &p, const vec3 &q) {
	return (q - p*cosh(hyper_distance(p, q))) / sinhf(hyper_distance(p, q));
}

static vec3 hyper_combination(const vec3 &p, const vec3 &q, float t) {
	auto d = hyper_distance(p, q);
	return (p * sinhf((1-t) * d) / sinhf(d) + q * sinhf(t * d) / sinhf(d));
}

static vec3 hyper_move(const vec3 &p, const vec3 &v, float d) {
	return p * coshf(d) + v * sinhf(d);
}

static vec3 hyper_mirror(const vec3 &p, const vec3 &m) {
	auto dist = hyper_distance(p, m);
	auto v = hyper_vector(p, m);
	return hyper_move(p, v, 2*dist);
}

// Initialization, create an OpenGL context
void onInitialization() {

	glViewport(0, 0, windowWidth, windowHeight);

	glGenVertexArrays(1, &points_vao);	// get 1 points_vao id
	glBindVertexArray(points_vao);		// make it active

	glGenBuffers(1, &points_vbo);	// Generate 1 buffer

	/***********************************************************************/

	glGenVertexArrays(1, &lines_vao);	// get 1 points_vao id
	glBindVertexArray(lines_vao);		// make it active

	glGenBuffers(1, &lines_vbo);	// Generate 1 buffer

	/***********************************************************************/

	pointsToVRam();

	// create program for the GPU
	gpuProgram.create(vertexSource, fragmentSource, "outColor");
}

// Window has become invalid: Redraw
void onDisplay() {
	glClearColor(0, 0, 0, 0);     // background color
	glClear(GL_COLOR_BUFFER_BIT); // clear frame buffer


	int location = glGetUniformLocation(gpuProgram.getId(), "color");
	glUniform3f(location, 0.0f, 0.0f, 1.0f); // 3 floats

	float MVPtransf[4][4] = { 1, 0, 0, 0,    // MVP matrix, 
							  0, 1, 0, 0,    // row-major!
							  0, 0, 1, 0,
							  0, 0, 0, 1 };

	location = glGetUniformLocation(gpuProgram.getId(), "MVP");	// Get the GPU location of uniform variable MVP
	glUniformMatrix4fv(location, 1, GL_TRUE, &MVPtransf[0][0]);	// Load a 4x4 row-major float matrix to the specified location

	glPointSize(8);
	glBindVertexArray(points_vao);  // Draw call
	glDrawArrays(GL_POINTS, 0 /*startIdx*/, 50 /*# Elements*/);

	// Set color to (0, 1, 0) = green
	location = glGetUniformLocation(gpuProgram.getId(), "color");
	glUniform3f(location, 1.0f, 0.0f, 0.0f); // 3 floats

	glBindVertexArray(lines_vao); // Draw call
	glDrawArrays(GL_LINES, 0, 100); // TODO: count-ot kiszamolni

	glutSwapBuffers(); // exchange buffers for double buffering
}

// Key of ASCII code pressed
void onKeyboard(unsigned char key, int pX, int pY) {
	if (key == 'd') glutPostRedisplay();         // if hyper_distance, invalidate display, i.e. redraw
}

// Key of ASCII code released
void onKeyboardUp(unsigned char key, int pX, int pY) {
}

// seg�d v�ltoz�, minimum ekkora mozg�sra van sz�ks�g
float enough_movement = .01f;

static vec3 calc_hyper_position(float cX, float cY) {
	return vec3(cX, cY, 1) / sqrt(1 - cX * cX - cY * cY);
}

void onClickedAndMove(vec3 curr_mouse_pos) {

	graph.shift(prev_mouse_pos, curr_mouse_pos);
	pointsToVRam();
	onDisplay();
	prev_mouse_pos = curr_mouse_pos;
}

// Move mouse with key pressed
void onMouseMotion(int pX, int pY) {	// pX, pY are the pixel coordinates of the cursor in the coordinate system of the operation system
	// Convert to normalized device space
	float cX = 2.0f * pX / windowWidth - 1;	// flip y axis
	float cY = 1.0f - 2.0f * pY / windowHeight;

	if (cX*cX + cY*cY > 1) { // ha nincs a k�rben, visszat�rek
		return;
	}

	auto curr = calc_hyper_position(cX, cY);
	if (hyper_distance(curr, prev_mouse_pos) > enough_movement)
		onClickedAndMove(curr);
}

// Mouse click event
void onMouse(int button, int state, int pX, int pY) { // pX, pY are the pixel coordinates of the cursor in the coordinate system of the operation system

	float cX = 2.0f * pX / windowWidth - 1;	// flip y axis
	float cY = 1.0f - 2.0f * pY / windowHeight;
	if (cX*cX + cY*cY > 1) {
		return;
	}

	prev_mouse_pos = calc_hyper_position(cX, cY);
	auto curr = calc_hyper_position(cX, cY);
	if (hyper_distance(prev_mouse_pos, curr) > enough_movement)
		onClickedAndMove(curr);

	/*char * buttonStat;
	switch (state) {
	case GLUT_DOWN: buttonStat = "pressed"; break;
	case GLUT_UP:   buttonStat = "released"; break;
	}

	switch (button) {
	case GLUT_LEFT_BUTTON:   printf("Left button %s at (%3.2f, %3.2f)\n", buttonStat, cX, cY);   break;
	case GLUT_MIDDLE_BUTTON: printf("Middle button %s at (%3.2f, %3.2f)\n", buttonStat, cX, cY); break;
	case GLUT_RIGHT_BUTTON:  printf("Right button %s at (%3.2f, %3.2f)\n", buttonStat, cX, cY);  break;
	}*/
}

// Idle event indicating that some time elapsed: do animation here
void onIdle() {
	long time = glutGet(GLUT_ELAPSED_TIME); // elapsed time since the start of the program
}