#!/bin/bash

dirname=$1

framework="./framework/"

mkdir -p "$dirname"

for ((i = 2; i <= $#; i++ )); do
  printf '%s\n' "Arg $i: ${!i}"
  mv "${!i}" "${dirname}"
done

wget -r -P "$dirname"/ -nH --cut-dirs=3 -R "index.html*" https://files.rethelyi.space/bme/grafika/framework/

name=$(echo $2 | cut -d. -f1)

sed -i "s/Skeleton/$name/g" ${dirname}/CMakeLists.txt
