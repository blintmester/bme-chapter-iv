% 1.
% Felület: r(u, v) = (u, v, uv)
% Mi a metrikus tenzor determinánsa az adott pontban?

u = 6;
v = 9;
r_u = [1, 0, v];
r_v = [0, 1, u];
I = [...
    dot(r_u, r_u), dot(r_u, r_v);
    dot(r_v, r_u), dot(r_v, r_v)
];
disp("1. A metrikus tenzor determinánsa:")
disp(det(I))

% 2.
% A felület a fenti
% Mennyi a Gauss-görbület az adott pontban?

u = 4;
v = 8;
r_u = [1, 0, v];
r_v = [0, 1, u];
r_uu = [0, 0, 0];
r_uv = [0, 0, 1];
r_vu = [0, 0, 1];
r_vv = [0, 0, 0];

N = cross(r_u, r_v);
N0 = N / norm(N);
I = [...
    dot(r_u, r_u), dot(r_u, r_v);...
    dot(r_v, r_u), dot(r_v, r_v);...
];
II = [...
    dot(N0, r_uu), dot(N0, r_uv);...
    dot(N0, r_vu), dot(N0, r_vv);...
    ];
disp("2. A Gauss-görbület: ")
disp(det(II)/det(I))

% 3.
% A felület a fenti
% N_x/N_z arány az adott pontban?

u = 6;
v = 2;
r_u = [1, 0, v];
r_v = [0, 1, u];
N = cross(r_u, r_v);
disp("3. N_x/N_z = ")
disp(N(1, 1)/N(1, 3))

% 4.
% A város: szélesség 45, hosszúság 168
% B város: szélesség 45, hosszúság 54
% A Föld sugara 6000 km
% Mekkora a távolság A és B között?

% Egységgömbi geometria ambiens térben:
lat = deg2rad(45);
lonA = deg2rad(168);
lonB = deg2rad(54);
A = [sin(lat)*cos(lonA), sin(lat)*sin(lonA),cos(lat)];
B = [sin(lat)*cos(lonB), sin(lat)*sin(lonB),cos(lat)];
dist = acos(dot(A, B));
% Skálázás a Föld sugarával
disp("4. A két város távolsága:");
disp(6000*dist);
% 5. Terep: h(x, y) = x^2 + 3*y^2
% v a szintvonal érintőjének irányvektora
% Mekkora az adott pontban a vy/vx arány?
x = 3;
y = 10;
disp("5. vy/vx = ")
disp(-x/(3*sqrt((5*x + 3*y*y - x^2)/3)))

% 6.
% A felület (u^2, v^2, u)
% Mennyi a metrikus tenzor determinánsa?

u = 8;
v = 3;
r_u = [2*u, 0, 1];
r_v = [0, 2*v, 0];

I = [...
    dot(r_u, r_u), dot(r_u, r_v);...
    dot(r_v, r_u), dot(r_v, r_v);...
];
disp("6. A metrikus tenzor determinánsa:")
disp(det(I))

% 7.
% A felület a fenti
% Mennyi a nagyobbik absz. értékű főgörbület absz. értéke?

u = 0;
v = 8;
r_u = [2*u, 0, 1];
r_v = [0, 2*v, 0];
r_uu = [2, 0, 0];
r_uv = [0, 0, 0];
r_vu = r_uv;
r_vv = [0, 2, 0];
N = cross(r_u, r_v);
N0 = N / norm(N);
I = [...
    dot(r_u, r_u), dot(r_u, r_v);...
    dot(r_v, r_u), dot(r_v, r_v);...
];
II = [...
    dot(N0, r_uu), dot(N0, r_uv);...
    dot(N0, r_vu), dot(N0, r_vv);...
    ];
S = II/I;
disp("7. A nagyobbik absz. értékű főgörbület absz. értéke:")
disp(max(abs(eig(S))))

% 8.
% A felület a fenti
% N_z/N_x arány az adott pontban?
u = 7;
v = 4;
r_u = [2*u, 0, 1];
r_v = [0, 2*v, 0];
N = cross(r_u, r_v);
disp("8. Az N_z/N_x arány az adott pontban:")
disp(N(1, 3)/N(1, 1))